package ru.dexsys.collectionsandgenerics

import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet
import kotlin.collections.LinkedHashMap
import kotlin.collections.LinkedHashSet

fun main(args: Array<String>) {
    //КОЛЛЕКЦИИ

    //1. В Kotlin все коллекции интерфейсно деляться на mutable и immutable, так же как и переменные
    //В Kotlin нет специальных синтаксических конструкций для создания списков или множеств
    //Для создания коллекций используйте либо классический java синтаксис, либо фабричные функции
    //стандартной библиотеки

    //1. Списки

    //List — когда важен порядок
    //List хранит и отслеживает позицию элементов.
    //Она знает, в какой позиции списка находится тот
    //или иной элемент, и несколько элементов могут
    //содержать ссылки на один объект

    //java-синтаксис
    var immutableList: List<Int> = ArrayList()
    //immutableList.add("something") - ошибка
    var mutableList: MutableList<String> = ArrayList()
    mutableList.add("zero")
    mutableList.add(1, "ten")
    mutableList.set(0, "two")
    mutableList.sort()
    mutableList.reverse()
    mutableList.addAll(listOf("one", "three"))
    mutableList.removeAt(1)
    mutableList.map { }

    //фабричные функции (создается ArrayList)
    immutableList = listOf(0, 1, 2, 3)
    mutableList = mutableListOf("one", "two", "three")

    //2. Сеты

    //Set — когда важна уникальность
    //Set не разрешает дубликаты и не отслеживает порядок, в котором хранятся значения. Коллекция
    //не может содержать несколько элементов, ссылающихся на один и тот же объект, или несколько
    //элементов, ссылающихся на два объекта, которые
    //считаются равными

    //Как это работает?

    // 1) Set получает хеш-код объекта и сравнивает
    // его с хеш-кодами объектов, уже находящихся
    // в множестве Set

    // 2) Set использует оператор === для сравнения
    // нового значения с любыми содержащимися
    // в нем объектами с тем же хеш-кодом

    // 3) Set использует оператор == для сравнения
    // нового значения со всеми объектами, содержащимися
    // в коллекции, с совпадающими хеш-кодами.

    //java-синтаксис
    var immutableSet: Set<Int> = LinkedHashSet()
    // LinkedHashSet - при итерации сохраняет порядок добавления
    //immutableSet.add("something") - ошибка
    var mutableSet: MutableSet<String> = HashSet()
    mutableSet.add("zero")
    var treeSet: TreeSet<Float> = TreeSet()
    treeSet.add(0.5F)
    //TreeSet это сеты с возможностью сортировки и сравнения элементов

    //фабричные функции (создается LinkedHashSet)
    immutableSet = setOf(0, 1, 2, 3)
    mutableSet = mutableSetOf("one", "two", "three")
    treeSet = sortedSetOf(0.1F, 0.2F, 0.3F)

    // 3. Мэпы

    //Map — когда важен поиск по ключу
    //Map использует пары «ключ-значение».
    //Этот тип коллекции знает, какое значение связано с заданным ключом. Два
    //ключа могут ссылаться на один объект,
    //но дубликаты ключей невозможны.
    //Хотя ключи обычно представляют собой строковые имена (например, для
    //составления списков свойств в формате
    //«имя-значение»), ключом также может
    //быть произвольный объект

    //java-синтаксис
    var immutableMap: Map<String, Int> = HashMap()
    var mutableMap: MutableMap<String, String> = LinkedHashMap()
    // LinkedHashMap - при итерации сохраняет порядок добавления
    var treeMap: SortedMap<Float, Char> = TreeMap()
    treeMap[0.6F] = 'H'
    //TreeMap это карты с возможностью сортировки и сравнения элементов

    //фабричные функции (сознается LinkedHashMap)
    immutableMap = mapOf("KEY_ONE" to 1, "KEY_TWO" to 2)
    mutableMap = mutableMapOf("KEY_ONE" to "one", "KEY_TWO" to "two")
    treeMap = sortedMapOf(0.4F to 'Y') //создает TreeMap с интерфейсом SortedMap

    // 4. immutable коллекции ковалентны, то есть вы можете присвоить список List<Bear> списку
    //List<Animal>
    val bears = listOf(Bear())
    val animals: List<Animal> = bears

    // 5. Функция-расширение toList позволяет привести mutableList список к immutable копированием
    val items = mutableListOf<String>()
    val immutableItems: List<String> = items.toList()
    // Аналогичные функции есть для maps и sets

    //ОБОБЩЕНИЯ

    // 1. Как и в Java, в Kotlin классы тоже могут иметь типовые параметры
    class Box<T>(t: T) {
        var value = t
    }

    val box: Box<Int> = Box<Int>(1)

    // 2. Ковариантность на месте объявления

    class Container<out T>(val contained: T)

    val container: Container<Number> = Container<Int>(25)
    val container2: Container<Number> = container
    // Не будет работать с var contained: T
    // Но это защищает от ошибок
    // container2.contained = 4.0

    // 3. Ковариантность на месте использования

    class OtherContainer<T : Any> {
        /*
            Пусть на момент создания контейнера мы еще не знаем,
            что конкретно он будет содержать, но знаем точно, что
            что-то с некоторым типом T он должен, следовательно после
            создания, рано или поздно, мы поменяем значение поля contained,
            а значит оно по определению не может быть val. В такой ситуации,
            мы не можем объявлять ковариантность на уровне класса
         */
        lateinit var contained: T

        override fun toString(): String {
            return "Container(contained=$contained)"
        }
    }

    fun createIntContainer(): OtherContainer<out Number> {
        val otherContainer = OtherContainer<Int>()
        otherContainer.contained = 42
        return otherContainer
    }

    fun createDoubleContainer(): OtherContainer<out Number> {
        val otherContainer = OtherContainer<Double>()
        otherContainer.contained = 42.0
        return otherContainer
    }

    // 4. Контрвариантность

    open class User(var login: String)

    class Worker(login: String) : User(login)

    class Admin(login: String) : User(login)

    class Printer<in T : User> {
        fun print(user: T) {
            println("User ${user.login} is logged in")
        }
    }

    fun printAdmin(printer: Printer<Admin>, admin: Admin) {
        printer.print(admin)
    }

    val admin = Admin("admin")
    printAdmin(Printer<User>(), admin)
    // Этот код не скомпилировался бы без in
    // применение этого модификатора справедливо как на уровне объявления,
    // так и на уровне использования

    // В java все это описывалось аббревиатурой PECS (Producer Extends, Consumer Super).
    // В kotlin - POCI (Producer Out, Consumer In)

    // 5.Star-projection

    class InvariantContainer<T>(val contained: T)

    fun process(container: InvariantContainer<*>) {
        println(container.contained)
        // мы можем печатать значения, т.к. знаем верхнюю границу обобщенного типа
    }
}

interface Animal

class Bear : Animal