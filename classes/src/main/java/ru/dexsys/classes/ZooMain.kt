package ru.dexsys.classes

import android.os.Parcelable
import androidx.core.os.bundleOf
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.random.Random.Default.nextInt

//КЛАССЫ, ИНТЕРСЕЙСЫ, НАСЛЕДОВАНИЕ, ПОЛИМОРФИЗМ

fun main(args: Array<String>) {
    val zoo = Zoo(space = 2000)
    val shark = Shark()
    zoo.placeHabitant(shark)
    //Object Expression. Об этом не в начале лекции
    //Для того, чтобы создать объект анонимного класса, который наследуется от какого-то типа (типов),
    //используется конструкция. Такой объект фактически является объектом класса inner
    zoo.placeHabitant(object : Predator(), LandHabitant {
        override val type: String = "Медведь"
        override val teeth: Int = 42

        override fun hunt() {
            println("Медведь ловит рыбу")
        }

        override val animalClass: AnimalClass = AnimalClass.MAMMALIA
    })
    zoo.placeHabitant(object : Animal {
        override val animalClass: AnimalClass = AnimalClass.REPTILE
        override val type: String = "Динозавр"
    })
    //Если всё-таки нам нужен просто анонимный объект
    val something = object {
        val type: String = "Не известно, что это такое"
    }
//  zoo.something - это не сработает!
}

//Классы в Kotlin объявляются с помощью использования ключевого слова class
//Объявление класса состоит из имени класса, заголовка (указания типов его параметров,
// основного конструктора и т.п) и тела класса, заключённого в фигурные скобки
//Класс в Kotlin может иметь основной конструктор (primary constructor) и один или более
//дополнительных конструкторов (secondary constructors). Основной конструктор является частью
// заголовка класса, его объявление идёт сразу после имени класса (и необязательных параметров)
class Zoo(space: Int) {
    //Модификаторы видимости
    //private означает видимость только внутри этого класса (включая его члены);
    //protected — то же самое, что и private + видимость в субклассах;
    //internal — любой клиент внутри модуля, который видит объявленный класс, видит и его internal члены;
    //public — любой клиент, который видит объявленный класс, видит его public члены.
    //Модификаторы доступа
    //open, inner
    private val stuff: Array<Human>
    private val enclosures: Array<Enclosure<out Habitant>>

    //При создании экземпляра класса блоки инициализации выполняются в том порядке, в котором они
    // идут в теле класса, чередуясь с инициализацией свойств
    init {
        stuff = Array(
            //параметры основного конструктора могут быть использованы в инициализирующем блоке
            space / 500
        ) {
            Human(
                name = it.toString(),
                gender = Gender.valueOfInt(nextInt(1, 2)),
                age = nextInt(20, 70),
                height = nextInt(150, 210),
                weight = nextInt(45, 150)
            )
        }
        enclosures = Array(space / 200) {
            when {
                it % 3 > 0 -> WaterEnclosure(
                    space = 150
                )
                it % 2 > 0 -> LandEnclosure(
                    space = 150
                )
                else -> MixedEnclosure(
                    space = 150
                )
            }
        }
    }

    fun placeHabitant(animal: Animal) {
        if (animal is Habitant) {
            for (enclosure in enclosures) {
                when {
                    animal is LandHabitant && enclosure is LandEnclosure -> {
                        enclosure.dweller = animal
                        println("${animal.type} помещен(а) в вольер для сухопутных животных. Материал ограждения - ${enclosure.wallMaterial}")
                        return
                    }
                    animal is WaterHabitant && enclosure is WaterEnclosure -> {
                        enclosure.dweller = animal
                        println("${animal.type} помещен(а) в вольер для водных животных. Материал ограждения - ${enclosure.wallMaterial}")
                        return
                    }
                    animal is MixedHabitant && enclosure is MixedEnclosure -> {
                        enclosure.dweller = animal
                        println("${animal.type} помещен(а) в вольер для животных со смешанной средой обитания. Материал ограждения - ${enclosure.wallMaterial}")
                        return
                    }
                }
            }
            println("Нет модходящего вольера!")
        } else {
            println("Не известно, в какой среде обитает ${animal.type}!")
        }
    }
}

class Human(
    //Для объявления и инициализации свойств основного конструктора в Kotlin есть лаконичное
    // синтаксическое решение
    //Свойства, объявленные в основном конструкторе, могут быть изменяемые и неизменяемые
    val name: String,
    val gender: Gender,
    var age: Int,
    var height: Int,
    var weight: Int
) : Animal, LandHabitant {
    val heart = Heart()
    val brain = Brain()
    var passport: Passport = Passport(hashCode().toLong(), name, Date())
    var nearestEnclosure: Enclosure<Habitant>? = null

    override val animalClass: AnimalClass
        get() = AnimalClass.MAMMALIA
    override val type: String = "Homo Sapiens"

    fun isMilitarian() = gender == Gender.MALE && age > 18 && age < 40

    fun isOld() = age > 65

    fun isBig() = height > 190

    fun isHeavy() = weight > 100

    fun openEnclosure() {
        nearestEnclosure?.isLocked = false
    }

    fun closeEnclosure() {
        nearestEnclosure?.isLocked = true
    }

    fun makePassportCopy() = passport.copy(date = Date(2021, 4, 8))

    fun sendPassport() = bundleOf("PASSPORT" to passport)

    //Классы могут быть вложены в другие классы
    class Heart {
        val valveCount: Int = 4

        fun pumpBlood() {
            println("Тук-тук, тук-тук")
        }
    }

    //Класс может быть отмечен как внутренний с помощью слова inner, тем самым он будет иметь доступ к
    // членам внешнего класса. Внутренние классы содержат ссылку на объект внешнего класса
    inner class Brain {
        val hemisphereCount: Int = 2

        fun controlHeart() {
            this@Human.heart.pumpBlood()
        }
    }
}

//Эта аннотация в сочетании с data class и интерфейсом Parcelable сильно упрощает жизнь android-разработчику
@Parcelize
//Такой класс называется классом данных. Компилятор автоматически формирует следующие члены данного
// класса из свойств, объявленных в основном конструкторе:
//пару функций equals()/hashCode(),
//функцию toString() в форме "Passport(number=464..555, name=Вася, date=01.01.2021)",
//компонентные функции componentN(), которые соответствуют свойствам, в соответствии с порядком их объявления,
//функцию copy()
//Есть стандартные Data классы - Pair и Triple
data class Passport(val number: Long, val name: String, val date: Date) : Parcelable {
    //В классах также могут быть объявлены дополнительные конструкторы
    constructor() : this(Long.MAX_VALUE, "", Date())
}

enum class Gender(val value: String) {
    MALE("Мужчина"),
    FEMALE("Женщина");

    //Object expression. Объявление объекта внутри класса может быть отмечено ключевым словом
    //companion
    //companion object может наследовать интерфейсы и классы
    //Не обязательно указывать имя вспомогательного объекта. В таком случае он будет назван Companion
    companion object {
        //Такая аннотация позволяет опустить обращение к компаньону и пользоваться методами объектов
        // как статикой даже из Java классов
        @JvmStatic
        fun valueOfInt(value: Int): Gender = if (value % 2 == 0) MALE else FEMALE
    }
}

abstract class Predator() : Animal {
    val eyes = 2
    abstract val teeth: Int

    fun eatMeat() {}

    abstract fun hunt()
}

class Shark() : Predator(), WaterHabitant {
    override val animalClass: AnimalClass = AnimalClass.FISH
    override val type: String = "Акула"

    override val teeth: Int = 1500

    override fun hunt() {
        println("Акула охотится за всеми живыми существами")
    }
}

abstract class Enclosure<H : Habitant>(val space: Int) {
    abstract val wallMaterial: Material

    var isLocked: Boolean = true
    var dweller: H? = null
}

class LandEnclosure(space: Int) : Enclosure<LandHabitant>(space) {
    override val wallMaterial: Material = Material.Steel(0.9F)
}

class WaterEnclosure(space: Int) : Enclosure<WaterHabitant>(space) {
    override val wallMaterial: Material = Material.Glass
}

class MixedEnclosure(space: Int) : Enclosure<MixedHabitant>(space) {
    override val wallMaterial: Material = Material.Wood("Дуб")
}

//Изолированные классы используются для отражения ограниченных иерархий классов, когда значение
// может иметь тип только из ограниченного набора, и никакой другой. Они являются, по сути,
// расширением enum-классов: набор значений enum типа также ограничен, но каждая enum-константа
// существует только в единственном экземпляре, в то время как наследник изолированного класса может
// иметь множество экземпляров, которые могут нести в себе какое-то состояние.
sealed class Material {
    class Steel(val carbonRate: Float) : Material() {
        override fun toString(): String = "Сталь $carbonRate"
    }

    class Wood(val type: String) : Material() {
        override fun toString(): String = "Дерево $type"
    }

    //Object expression. Таким образов в kotlin объявляется Singleton
    //Это называется объявлением объекта и всегда имеет приставку в виде ключевого слова object.
    // Аналогично объявлению переменной, объявление объекта не является выражением и не может быть
    // использовано в правой части оператора присваивания
    //Объявление объекта не может иметь локальный характер
    object Glass : Material() {
        override fun toString(): String = "Стекло"
    }

    fun burn(material: Material): String =
        when (material) {
            is Steel -> "Плавится"
            is Wood -> "Поддерживает горение"
            is Glass -> "Лопается"
            //Ключевое преимущество от использования изолированных классов проявляется тогда, когда
            //вы используете их в выражении when. Если возможно проверить что выражение покрывает все
            //случаи, то вам не нужно добавлять else.
        }


    fun toWet(material: Material): String =
        when (material) {
            is Steel -> "Ржавеет"
            is Wood -> "Разбухает"
            is Glass -> "Ничего не происходит"
        }
}

interface Animal {
    val animalClass: AnimalClass
    val type: String

    fun getDomainName(): String = "Eukaryota"
}

enum class AnimalClass {
    FISH,
    AMPHIBIA,
    BIRD,
    REPTILE,
    MAMMALIA
}

interface Habitant

interface WaterHabitant : Habitant

interface LandHabitant : Habitant

interface MixedHabitant : Habitant
