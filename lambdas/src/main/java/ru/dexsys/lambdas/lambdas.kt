package ru.dexsys.lambdas

import java.util.*
import kotlin.math.pow

/**
 * Created by Grigorii Shadrin on 11.04.2021.
 * Project Kotlin Android Development
 *
 */

fun main(args: Array<String>) {
    //ФУНКЦИИ ВЫСШЕГО ПОРЯДКА И ЛЯМБДЫ

    //В Kotlin функции являются функциями первого класса. Это означает, что они могут храниться в
    // переменных и структурах данных, передаваться в качестве аргументов и возвращаться из других
    // функций высшего порядка

    // 1. Лямбда-выражения и анонимные функции

    // Лямбда-выражения и анонимные функции - это "функциональный литерал", то есть необъявленная
    // функция, которая немедленно используется в качестве выражения
    val list = listOf(1, 2, 3)
    val filteredList = list.filter { it -> it > 1 }
    // Здесь it -> it > 1 - лямбда-выражение, которое будет использовано как предикат во время
    // итерирования списка

    // Синтаксис лямбда-выражений
    val sum = { x: Int, y: Int -> x + y }
    //        {    параметры   -> тело  }

    // trailing lambda
    val accumulator = list.fold(1) { acc, e -> acc * e }

    // Если лямбда единственный аргумент ФВП, то круглые скобки можно опустить
    run { }

    // it - неявное имя единственного параметра лямбда-выражения
    val strings: List<String> = list.map { it.toString() }

    // Возвращать значение можно return, а можно без него (последнее значение в блоке)
    // Таким образом получаем программирование в стиле .NET Language-Integrated Query
    strings.filter { it.length == 1 }.sortedBy { it }.map { it.toUpperCase() }

    // Анонимные функции отличаются от лямбда-выражений только тем, что позволяют явно вывести тип
    val simpleFunc = fun(x: Int, y: Int): Int = x + y

    // Тип параметра может быть опущен
    list.filter(fun(item) = item > 0)

    // Замыкание. Лямбды имею доступ к своему замыканию, т.е. к переменным вне области их тела
    var budget = 10
    strings.map { it.toInt() }.forEach {
        budget += -it
    }
    println(budget)

    // 2. Функциональные типы
    // 1) У всех функциональных типов есть список с типами параметров, заключенный в скобки,
    // и возвращаемый тип: (A, B) -> C
    // Список с типами параметров может быть пустым, как, например, в () -> A

    // 2) У функциональных типов может быть дополнительный тип - получатель (receiver), который
    // указывается в объявлении перед точкой: тип A.(B) -> C описывает функции, которые могут быть
    // вызваны для объекта-получателя A с параметром B и возвращаемым значением C

    //Создание функционального типа
    //1)Лямбда-выражением
    val lambdaVariable = { one: String, two: String, three: String -> println("$one $two $three") }
    //2)Анонимной функцией
    val anonymousVariable = fun(str: Int, dex: Int, con: Int): Int { return (str + dex) / con }
    //3)Функцией с объектом-приёмником
    val receiverVariable = fun Int.(dex: Int, con: Int): Int = (this + dex) / con
    //4)Из функции, свойства или конструктора
    val referenceVariable: (Array<String>) -> Unit = ::main
    val regexLambda: (String) -> Regex = ::Regex

    //5)Используя экземпляр пользовательского класса,
    // который реализует функциональный тип в качестве интерфейса:
    class IntPower : (Int) -> Double {
        override operator fun invoke(x: Int): Double = x.toDouble().pow(2.0)
    }

    val byClass: (Int) -> Double = IntPower()

    // 3. Значение функционального типа может быть вызвано с помощью оператора invoke(...):
    // f.invoke(x) или просто f(x)
    lambdaVariable("раз", "два", "три")
    println(anonymousVariable.invoke(4, 5, 6))

    // С помощью ФВП можно делать достаточно компактные и хорошо читаемые функции обратного вызова
    val listener: (Int) -> Unit = { println("onClick with result: $it") }

    listener.invoke(42)

    // 4. Функции высшего порядка
    // С помощью ФВП вы можете создавать методы, которые будут использоваться в деклоративном стиле
    fun <T> T.printIf(predicate: (T) -> Boolean) {
        if (predicate(this)) println(this)
    }

    "Карандаш".printIf { it.contains("ш") }

    //Популярная функция стандартной библиотеки с типом получателем
    fun <T> T.apply(block: T.() -> Unit): T {
        block() //нет аргумента
        return this
    }

    //Точно такая же функция, но без типа получателя
    fun <T> T.also(block: (T) -> Unit): T {
        block(this) //аргумент с типом T
        return this
    }
    //Данный пример наглядно показывает разницу между простыми лямбда функциями
    // и лямбдами с типом получателя
    println(Date().apply {
        hours = 6
        minutes = 6
        seconds = 6
    })

    println(Date().also {
        it.hours = 1
        it.minutes = 1
        it.seconds = 1
    })

    // 5. Функции высшего порядка стандартной библиотеки
    val numbersAsString = listOf("1", "2", "3", "4", "5", "4", "la-la-la")

    val awesomeResult = numbersAsString
            //С помощью map{} трансформируем каждый элемент коллекции. Так же обратите внимание на
            //функцию mapIndexed{}, forEach{}, forEachIndexed{}
            .map {
                try {
                    it.toInt()
                } catch (e: Throwable) {
                    null
                }
            }
            //С помощью filterNotNull избавляемся от null в коллекции. Так же стоит обратить внимание
            //на функцию filterIsInstance
            .filterNotNull()
            //С помощью filter{} делаем фильтрацию по предикату
            .filter { it % 2 == 0 }
            //С помощью any{} определяем есть ли в списке хотя бы один элемент удовлетворяющий предикату.
            //Посмотрите самостоятельно функции find{}, all{}
            .any { it == 4 }
    println(awesomeResult)

    class SampleClass {
        val recursiveVariable: SampleClass by lazy { SampleClass() }
        lateinit var text: String
    }

    val hahaha = SampleClass()
    //С помощью with вы можете опустить использование оператора . (точка). Самостоятельно посмотрите
    //also{}, apply{}, takeIf{}, run{}
    with(hahaha.recursiveVariable.recursiveVariable.recursiveVariable.recursiveVariable.recursiveVariable.recursiveVariable) {
        text = "Это бывает очень удобно"
        println(text)
    }
}