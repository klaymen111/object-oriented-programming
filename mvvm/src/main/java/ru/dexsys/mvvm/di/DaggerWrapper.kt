package ru.dexsys.mvvm.di

import ru.dexsys.mvvm.MVVMComponent
import ru.dexsys.mvvm.MVVMDependencies
import ru.dexsys.mvvm.presentation.ImportantActivity

// Обертка над Dagger для модуля MVVM
object DaggerWrapper {

    private lateinit var graph: ModuleGraph
// Метод для инициализации графа
    fun init(dependencies: MVVMDependencies) {
        graph = DaggerModuleGraph.builder()
                //добавляем в граф зависимости из нашего компонента
                .plus(dependencies)
                .build()
    }
// Метод для внедрения зависимостей в MVVMComponent
    fun inject(component: MVVMComponent) {
        if (DaggerWrapper::graph.isInitialized) {
            graph.inject(component)
        }
    }
// Метод для внедрения зависимостей в ImportantActivity
    fun inject(activity: ImportantActivity) {
        if (DaggerWrapper::graph.isInitialized) {
            graph.inject(activity)
        }
    }
}