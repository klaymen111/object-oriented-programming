package ru.dexsys.mvvm.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import ru.dexsys.mvvm.presentation.ImportantViewModel
import ru.dexsys.mvvm.presentation.resourceproviders.ImportantResourceProvider
import ru.dexsys.mvvm.presentation.resourceproviders.ImportantResourceProviderImpl
import ru.dexsys.mvvm.MVVMApi
import ru.dexsys.mvvm.MVVMApiImpl

@Module(includes = [ImportantActivityModule.InstanceModule::class])
interface ImportantActivityModule {

    @Binds
    fun bindsMVVMApi(api: MVVMApiImpl): MVVMApi

    @Binds
    fun bindsImportantResourceProvider(impl: ImportantResourceProviderImpl): ImportantResourceProvider

    @Module
    class InstanceModule {
        @Provides
        fun providesImportantViewModel(resourceProvider: ImportantResourceProvider) =
            ImportantViewModel(resourceProvider)
    }
}


