package ru.dexsys.mvvm.di

import android.app.Application
import dagger.Component
import ru.dexsys.mvvm.MVVMComponent
import ru.dexsys.mvvm.MVVMDependencies
import ru.dexsys.mvvm.presentation.ImportantActivity
import javax.inject.Singleton

//Описание графа модуля

@Singleton
@Component(
    modules = [ImportantActivityModule::class],
    dependencies = [MVVMDependencies::class]
)
interface ModuleGraph {

    //Внедряем зависимости в component
    fun inject(component: MVVMComponent)
    //Внедряем зависимости в activity
    fun inject(activity: ImportantActivity)

    @Component.Builder
    interface Builder {
        //Добавляем внешние зависимости модуля
        fun plus(dependencies: MVVMDependencies): Builder
        fun build(): ModuleGraph
    }
}