package ru.dexsys.mvvm

import android.content.Context
import ru.dexsys.mvvm.di.DaggerWrapper
import javax.inject.Inject

//Компонент это сущность, которая хранит имплементацию api и инициализирует граф
class MVVMComponent(dependencies: MVVMDependencies) {

    @Inject
    lateinit var api: MVVMApi

    init {
        //При создании компонента создаем граф зависимостей
        DaggerWrapper.init(dependencies)
        //Тут же внедряем api в компонент
        DaggerWrapper.inject(this)
    }
}

interface MVVMDependencies {
    val context: Context

    fun getHelloAndroidActions(): HelloAndroidActions
}

interface HelloAndroidActions {
    fun navigateToStartActivity(context: Context)
}
