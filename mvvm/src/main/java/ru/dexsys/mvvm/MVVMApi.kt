package ru.dexsys.mvvm

import android.content.Context
import android.content.Intent
import ru.dexsys.mvvm.presentation.ImportantActivity
import javax.inject.Inject

// api модуля MVVM
interface MVVMApi {
    fun navigateToImportantActivity(context: Context)
}

class MVVMApiImpl @Inject constructor() : MVVMApi {
    override fun navigateToImportantActivity(context: Context) {
        context.startActivity(Intent(context, ImportantActivity::class.java))
    }
}