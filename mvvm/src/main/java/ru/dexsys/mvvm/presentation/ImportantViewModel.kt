package ru.dexsys.mvvm.presentation

import android.view.View
import android.widget.ImageView
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import ru.dexsys.mvvm.BR
import ru.dexsys.mvvm.presentation.resourceproviders.ImportantResourceProvider
import javax.inject.Inject

class ImportantViewModel @Inject constructor(private val resourceProvider: ImportantResourceProvider) :
    BaseObservable() {

    private var counter = 0
    private val urls = listOf(
        "https://randomwordgenerator.com/img/picture-generator/53e5d7474354ad14f1dc8460962e33791c3ad6e04e5074407c2e7bd09f49c4_640.jpg",
        "https://randomwordgenerator.com/img/picture-generator/53e3d1454855ae14f1dc8460962e33791c3ad6e04e507440702d79d39f4dc3_640.jpg",
        "https://randomwordgenerator.com/img/picture-generator/57e6d543485bae14f1dc8460962e33791c3ad6e04e507440762a7cd5914dcd_640.jpg",
        "https://randomwordgenerator.com/img/picture-generator/53e2d7424353a814f1dc8460962e33791c3ad6e04e50744075287cdd9e49c4_640.jpg",
        "https://randomwordgenerator.com/img/picture-generator/57e9d5444e5ba814f1dc8460962e33791c3ad6e04e50744172287ad2944cc3_640.jpg",
        "https://randomwordgenerator.com/img/picture-generator/57e9d2454f51a414f1dc8460962e33791c3ad6e04e507441722872d69f4bc6_640.jpg",
        "https://randomwordgenerator.com/img/picture-generator/55e5d1424951ab14f1dc8460962e33791c3ad6e04e50744172297ed2914cc2_640.jpg",
        "https://randomwordgenerator.com/img/picture-generator/53e1d7444d53af14f1dc8460962e33791c3ad6e04e507440742a7ad19f4cc0_640.jpg",
        "https://randomwordgenerator.com/img/picture-generator/55e1d0454e56ac14f1dc8460962e33791c3ad6e04e50744172287ed2914ac0_640.jpg",
        "https://randomwordgenerator.com/img/picture-generator/52e8d3434f54af14f1dc8460962e33791c3ad6e04e507441722872d7934ac0_640.jpg"
    )

    @Bindable
    var counterText = resourceProvider.thisButtonWasNotPressedText
        set(value) {
            field = value
            notifyPropertyChanged(BR.counterText)
        }

    @Bindable
    var url = urls.random()
        set(value) {
            field = value
            notifyPropertyChanged(BR.url)
        }

    fun onButtonCounterClick(view: View) {
        counter += 1
        url = urls.random()
        counterText = resourceProvider.getButtonWasPressedSomeRounds(counter)
    }

    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun setImageUrl(view: ImageView, url: String) {
            Glide.with(view.context)
                .load(url)
                .circleCrop()
                .into(view) }
    }
}