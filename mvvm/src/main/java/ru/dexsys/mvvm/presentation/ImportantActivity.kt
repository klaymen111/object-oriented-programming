package ru.dexsys.mvvm.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import ru.dexsys.mvvm.R
import ru.dexsys.mvvm.databinding.ActivityImportantBinding
import ru.dexsys.mvvm.di.DaggerWrapper
import javax.inject.Inject

//Наша View мало чем отличается View c паттерном MVP
class ImportantActivity : AppCompatActivity() {

    //Внедряем ViewModel
    @Inject
    lateinit var importantViewModel: ImportantViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerWrapper.inject(this)
        super.onCreate(savedInstanceState)
        //Инфлейт выполняется специальным классом DataBindingUtil
        val binding: ActivityImportantBinding = DataBindingUtil.setContentView(
            this, R.layout.activity_important
        )
        //Привязываем нашу ViewModel к Model
        binding.viewModel = importantViewModel
    }
}