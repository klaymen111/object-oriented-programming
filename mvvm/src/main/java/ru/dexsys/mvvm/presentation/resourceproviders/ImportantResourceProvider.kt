package ru.dexsys.mvvm.presentation.resourceproviders

import android.content.Context
import ru.dexsys.mvvm.R
import javax.inject.Inject
import kotlin.math.abs

interface ImportantResourceProvider {
    val thisButtonWasNotPressedText: String

    fun getButtonWasPressedSomeRounds(counter: Int): String
}

class ImportantResourceProviderImpl @Inject constructor(private val context: Context) :
    ImportantResourceProvider {
    override val thisButtonWasNotPressedText: String by lazy { context.getString(R.string.mvvm_this_button_was_not_pressed) }

    override fun getButtonWasPressedSomeRounds(counter: Int): String = context.getString(
        R.string.mvvm_button_was_pressed_some_rounds,
        context.resources.getQuantityString(
            R.plurals.rounds,
            abs(counter),
            counter
        )
    )
}
