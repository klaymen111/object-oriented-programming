package ru.dexsys.application.mediators

//Синглтон для хранения медиаторов
object MediatorManager {
    val helloAndroidMediator: HelloAndroidMediator by lazy { HelloAndroidMediator() }
    val mvvmMediator: MVVMMediator by lazy { MVVMMediator() }
}