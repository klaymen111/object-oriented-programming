package ru.dexsys.application.mediators

import android.content.Context
import ru.dexsys.application.ComponentHolder
import ru.dexsys.mvvm.HelloAndroidActions
import ru.dexsys.mvvm.MVVMApi
import ru.dexsys.mvvm.MVVMComponent
import ru.dexsys.mvvm.MVVMDependencies
import java.lang.ref.WeakReference

//Медиатор модуля MVVM
class MVVMMediator {
    private var context: WeakReference<Context>? = null

    val api: MVVMApi = object : MVVMApi {
        override fun navigateToImportantActivity(context: Context) {
            this@MVVMMediator.context = WeakReference(context)
            component.api.navigateToImportantActivity(context)
        }
    }

    private val componentHolder: ComponentHolder<MVVMComponent, MVVMDependencies> =
        ComponentHolder { MVVMComponent(it) }

    private val component: MVVMComponent
        get() {
            return componentHolder.provideComponent {
                object : MVVMDependencies {
                    override val context: Context
                        get() = this@MVVMMediator.context?.get()
                            ?: throw Exception("Нет контекста!")

                    override fun getHelloAndroidActions(): HelloAndroidActions =
                        object : HelloAndroidActions {
                            override fun navigateToStartActivity(context: Context) {
                                MediatorManager.helloAndroidMediator.api.navigateToStartActivity(
                                    context
                                )
                            }
                        }
                }
            }
        }
}
