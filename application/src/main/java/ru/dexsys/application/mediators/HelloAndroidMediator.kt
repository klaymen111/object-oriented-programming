package ru.dexsys.application.mediators

import android.content.Context
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import ru.dexsys.application.ComponentHolder
import ru.dexsys.helloandroid.HelloAndroidComponent
import ru.dexsys.helloandroid.HelloAndroidDependencies
import ru.dexsys.helloandroid.MVVMActions

//Сущность для взаимодействия модуля HelloAndroid с другими модуля
class HelloAndroidMediator {

    //здесь хранится компонент модуля
    private val componentHolder: ComponentHolder<HelloAndroidComponent, HelloAndroidDependencies> =
        ComponentHolder { HelloAndroidComponent(it) }

    private val component: HelloAndroidComponent
        get() {
            return componentHolder.provideComponent {
                //объявляем внешние зависимости при создании компонента
                object : HelloAndroidDependencies {
                    override fun getMVVMActions(): MVVMActions =
                        object : MVVMActions {
                            override fun navigateToImportantActivity(context: Context) {
                                //удовлетворить внешние зависимости можно только через медиаторы других модулей
                                //за реализацию этой зависимости отвечает модуль MVVM
                                MediatorManager.mvvmMediator.api.navigateToImportantActivity(context)
                            }
                        }
                }
            }
        }

    val api: Api = object : Api {
        override fun init(app: DaggerApplication): AndroidInjector<out DaggerApplication> =
            //мы сохранили dagger android для модуля helloandroid, но делегировали функцию инициализации компоненту модуля
            component.init(app)

        override fun navigateToStartActivity(context: Context) {
            //Api модуля имплементировано непосретственно в компоненте целевого модуля
            component.api.navigateToHelloAndroidActivity(context)
        }
    }
}

//Api объявленное в медиаторе является лишь фасадом и не обязательно должно соответствовать сигнатуре цевого модуля
interface Api {
    fun init(app: DaggerApplication): AndroidInjector<out DaggerApplication>
    fun navigateToStartActivity(context: Context)
}
