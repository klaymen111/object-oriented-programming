package ru.dexsys.application

import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerApplication
import ru.dexsys.application.mediators.MediatorManager
import javax.inject.Inject

// Базовый класс для поддержания глобального состояния приложения. Нужно указать имя класса в качестве
// атрибута «android:name» в теге application AndroidManifest.xml. Объект класса Application или
// наследника класса Application создается перед любым другим экземпляром при создании процесса
// для вашего приложения
// Интерфейс HasAndroidInjector содержит метод androidInjector(): AndroidInjector<Any>
class MainApplication : DaggerApplication() {
    // Здесь делаем иньекцию в поле и храним наш инжектор
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        MediatorManager.helloAndroidMediator.api.init(this)
}