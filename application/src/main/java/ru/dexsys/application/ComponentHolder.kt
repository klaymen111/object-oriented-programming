package ru.dexsys.application

//Класс для хранения и инициализации компонента
class ComponentHolder<Comp, Deps>(val param: (Deps) -> Comp) {

    private var component: Comp? = null

    fun provideComponent(componentBuilder: () -> Deps): Comp {
        if (!hasComponent()) {
            initComponent(componentBuilder())
        }
        return component!!
    }

    private fun hasComponent(): Boolean = component != null

    private fun initComponent(deps: Deps) {
        component = param(deps)
    }

    fun destroyComponent() {
        component = null
    }
}