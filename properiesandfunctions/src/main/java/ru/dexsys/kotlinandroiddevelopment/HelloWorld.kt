package ru.dexsys.kotlinandroiddevelopment

import kotlin.math.pow

/**
 * Created by Grigorii Shadrin on 01.04.2021.
 * Project Kotlin Android Development
 *
 */
//ПЕРЕМЕННЫЕ

//константа всегда immutable и должна быть инициализирована сразу
const val PI: Float = 3.14159F

//у этой mutable переменной тип определился автоматически. Не путайте с var из C#!
var globalScopeVariable = "глобальная переменная"

//nullable тип переменной всегда задается в явном виде
var nullableVariable: Any? = "anything"

//инициализация mutable переменной можно отложить модификатором lateinit
lateinit var lateInitGlobalVariable: Array<Int>

//каждая переменная имеет теневое поле и методы доступа к нему
//var имеет методы get и set, которые можно заменить своими реализациями (override)
var trickyVariable: Int = 0
    get() {
        println(field)
        return field
    }
    set(value) {
        field = value - 1
    }

//val имеет только get
val stupidVariable: Char
    get() = '^'

//в kotlin есть простой способ сделать "ленивую инициализацию"
val veryLazyVariable by lazy { "I so laazy" }

fun main(args: Array<String>) {
    //если вы не уверены, что lateinit переменная была инициализована, то сделайте проверку
    //кстати, в kotlin нет тернарного оператора, но есть короткая форма оператора ветвления if
    if (::lateInitGlobalVariable.isInitialized) println("cool!") else println("sad(")
    println(veryLazyVariable)

    //ВНУТРЕННИЕ ПЕРЕМЕННЫЕ

    //для отложенной инициализации внутренних переменных не нужен lateinit
    //будьте внимательны!
    //тип обязателен, когда значение не инициализируется, т.к. типизация статическая
    val innerVariable: Int
    innerVariable = 42 // последующее присвоение

    //МАССИВЫ

    //Массивы можно создавать библиотечными функциями
    val newArray: Array<Float>
    //?
    val nullsArray = arrayOfNulls<String>(50)
    nullsArray[0] = ""
    val asc = Array(5, { i -> (i * i).toString() })

    //Массивы в kotlin инвариантны, т.е. нельзя написать так
    //val anyArray: Array<Any> = newArray

    //Для примитивных типов есть типизированные массивы, которые позволяют не использовать боксинг
    val x: IntArray = intArrayOf(1, 2, 3)
    x[0] = x[1] + x[2]

    //СТРОКА

    //kotlin поддерживает классическую строковую конкатенацию
    println("Hello" + " " + "Kotlin!")
    //а так же более удобные строковые шаблоны
    println("смысл жизни = $innerVariable")
    //внутри шаблона можно даже писать код, если это очень нужно
    val dream = "Лучше, конечно, поддерживать код максимально читаемым"
    println("Что-что?\n${dream.replace("поддерживать", "не поддерживать")}")

    //ГЛОБАЛЬНЫЕ ФУНКЦИИ

    ourAwesomeFunction()
    val circleAria = getCircleArea(15)
    var circlesAriaSum = getCirclesAria(circleAria, 50)
    //в kotlin есть очень удобная конструкция - именованные аргументы
    circlesAriaSum = getCirclesAria(circleAria = circleAria, count = 55, color = "Зеленый")

    nextMain(args)

}

//так выглядит функция, которая ничего не возвращает
fun ourAwesomeFunction() {
    println("Наша восхитительная функция!")
}

//Функции можно записывать в одну строку
fun getCircleArea(radius: Int): Double = 2 * PI * (radius.toDouble().pow(2.0))

//внутри функций можно определять другие функции
fun getCirclesAria(circleAria: Double, count: Int): Double {
    fun redundantFunction(circleAria: Double, count: Int): Double {
        println("Можно было и не городить тут это!")
        return circleAria * count
    }
    getVoid()
    return redundantFunction(circleAria, count)
}

//на самом деле её можно было бы записать так. Unit - то же что и Void в Java и C#
fun getVoid(): Unit {
    println("Мне нечего тебе дать")
}

//функции можно перегружать, как и раньше
fun getCirclesAria(circleAria: Double, count: Int, color: String): Double {
    println("$color мой любимый цвет!")
    return getCirclesAria(circleAria, count)
}

fun nextMain(args: Array<String>) {
//ИСПОЛЬЗОВАНИЕ УСЛОВНЫХ ВЫРАЖЕНИЙ

    //Вы можете записывать условия в классическом для java и C# стиле
    fun max(a: Int, b: Int): Int {
        if (a > b)
            return a
        else
            return b
    }

    //Но вообще-то в kotlin почти все операторы являются функциями!
    fun alterMax(a: Int, b: Int) = if (a > b) a else b

    //В kotlin есть стандартная else if конструкция
    val variable = 333
    var result = if (variable < 0) {
        "to less"
    } else if (variable > 333) {
        "to big"
    } else {
        "fine"
    }

    //Но гораздо удобнее зачастую использовать оператор when
    result = when {
        variable < 0 -> "to less"
        variable > 333 -> "to big"
        else -> "fine"
    }

    //Кстати, в kotlin нет конструкции switch case. Вместо него тоже when
    result = when (variable) {
        0 -> "to less"
        333 -> "to big"
        else -> "fine"
    }

    //Прямо в операторе when можно объявить переменную. Это бывает удобно
    when (val localVariable = globalScopeVariable) {
        "глобальная переменная" -> println(localVariable)
        else -> return
    }

    getStringLength("ddd")
}

//ПРОВЕРКА ТИПА И АВТОМАТИЧЕСКОЕ ПРИВЕДЕНИЕ ТИПОВ

// Оператор is проверяет, является ли выражение экземпляром заданного типа.
// Если неизменяемая внутренняя переменная или свойство уже проверены на определенный тип,
// то в дальнейшем нет необходимости явно приводить к этому типу
fun getStringLength(obj: Any): Int? {
    if (obj is String) {
        // в этом блоке `obj` автоматически преобразован в `String`
        return obj.length
    }

    // `obj` имеет тип `Any` вне блока проверки типа
    return null
}

//или так
fun getAlterStringLength(obj: Any): Int? {
    if (obj !is String)
        return null

    // в этом блоке `obj` автоматически преобразован в `String`
    return obj.length
}

//или даже так
fun getAlterTwoStringLength(obj: Any): Int? {
    // `obj` автоматически преобразован в `String` справа от оператора `&&`
    if (obj is String && obj.length > 0)
        return obj.length

    return null
}

//ЦИКЛЫ

//Самый просто цикл в kotlin это цикл while
fun printArgsWithWhile(args: Array<String>) {
    var i = 0
    while (i < args.size) {
        print(args[i++])
    }
}

//Цикл for имеет расширенный функционал
fun printArgsWithFor(args: Array<String>) {
    for (arg in args) {
        print(arg)
        //Показать итератор
    }
}

//Имеет вариант конструкции с индексом итератора
fun printArgsWithIndexedFor(args: Array<String>) {
    for ((index, value) in args.withIndex()) {
        println("the element at $index is $value")
    }
}

//РАБОТА С ИНТЕРВАЛАМИ
fun forTheIntervals(array: Array<String>) {

    //Проверка на вхождение числа в интервал
    val x = 0
    val y = 10
    if (x in 1..y - 1)
        print("OK")

    //Проверка значения на выход за пределы интервала:

    if (x !in 0..array.lastIndex)
        print("Out")
    //Перебор значений в заданном интервале:

    for (k in 1..5)
        print(k)
    //Или по арифметической прогрессии:

    for (l in 1..10 step 2) {
        print(l)
    }
    for (m in 9 downTo 0 step 3) {
        print(m)
    }
}
