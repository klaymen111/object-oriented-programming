package ru.dexsys.nullsafeandecxeptions

import java.lang.Integer.parseInt

fun main(args: Array<String>) {
    //NULLABLE ТИП

    //По умолчанию типы
    //Kotlin не поддерживают значения null. Если вам нужна переменная,
    //способная хранить null, вы должны явно указать, что ее тип
    //является null-совместимым.
    var string: String? = ""
    //Удаление ссылки на объект с использованием null
    string = null
    //В случае, если вы захотите получить доступ к значению string, это будет небезопасно.
    // Компилятор предупредит об ошибке string.length - так делать нельзя

    //ПРОВЕРКА НА NULL

    //1. Явная проверка оператором ветвления
    var stringLength = if (string != null) string.length else -1

    //2. Оператор безопасного вызова ?.
    //В данном примере nullableStringLength имеет тип Int?, т.к. ?. это то же самое, что if(string != null) string else null
    var nullableStringLength = string?.length
    //цепочку ?. можно продолжать сколько угодно, опуская операторы ветвления
    nullableStringLength = string?.substring(1)?.replace("", "1")?.length

    //3. Если вам нужно использовать nullable тип, как non-null для промежуточных вычислений или в
    //качестве аргумента метода, используйте конструкцию ?.let {}
    val nullableString: String? = null
    nullableStringLength = nullableString?.let { it.length + 2 }
    // внутри блока let объект неявно присваивается к
    //локальному полю val
    nullableString?.let {
        if (it.isEmpty()) {
            println(nullableStringLength)
        }
    }

    //4. Если нам нужно преобразовать nullable переменную в non-null, для этого используется
    // специальный оператор "Элвис" - ?:
    val nonNullString: String = nullableString ?: "" //фактически он заменяет оператор ветвления if
    val nonNullInt: Int = nullableString?.length ?: 0 //удобно сочетать использование оператора
    // безопасного вызова с оператором Элвис

    //5. Не рекомендую это использовать, но в kotlin есть ещё и оператор небезопасного вызова
    var dangerousString: String? = "Dangerous!"
    println(dangerousString!!.length) //использование !! не идеоматично для kotlin и не гарантирует
    // безопасность работы пашей программы
    dangerousString = null
    //Дело в том, что nullable переменная в любой момент времени может принять
    // значение null, как в этом примере. Если это произойдет до !!, будет выброшено NPE

    //6. SAFE CASTS. Обычное приведение типа может вызвать ClassCastException в случае, если объект
    //имеет другой тип. Можно использовать безопасное приведение, которое вернёт null,
    //если попытка не удалась
    val mysteriousString: Any = "be careful"
    val checkedString: String? = mysteriousString as? String

    //7. Приведение nullable массивов (коллекций) к non-null типу
    val nullableArray: Array<String?> = arrayOf(null, "1", "2", null)
    //Для этого существует фабричный метод filterNotNull(), но возвращаемый тип только интерфейс List
    val nonNullArray: Array<String> = nullableArray.filterNotNull().toTypedArray()
    for (s in nonNullArray)
        println(s)

    //ИСКЛЮЧЕНИЯ

    //1. Исключения в Kotlin являются наследниками класса Throwable. У каждого исключения есть
    //сообщение, трассировка стека, а также причина, по которой это исключение вероятно было вызвано
    //Для того, чтобы возбудить исключение явным образом, используйте оператор throw
    fun doThrowableAction() {
        throw Exception("Исключение на ровном месте!")
    }

    //2. Как и в java, оператор try позволяет перехватывать и обрабатывать исключения
    try {
        throw IllegalArgumentException("Это совершенно нелегально")
    } catch (e: Throwable) {
        if (e is java.lang.IllegalArgumentException) {
            println(e.message)
        }
    } finally {
        println("Буду исполняться в любом случае!")
    }
    //В целом, блоки catch и finally можно опустить, но должен остаться хотя бы один

    //3. Try это выражение и оно может иметь возвращаемое значение
    val input = "123"
    val throwableVariable: Int = try {
        parseInt(input)
    } catch (e: NumberFormatException) {
        0
    }

    //4. Вы можете использовать выражение throw в качестве части элвис-выражения
    val intOrNothing =
        nullableString?.length ?: throw NullPointerException("nullableString is null!")
    //Типом выражения throw является специальный тип под названием Nothing.
    //Он используется для того, чтобы обозначить те участки кода, которые могут быть не
    // достигнуты никогда
    fun fail(message: String): Nothing {
        throw NullPointerException(message)
    }

    val s = nullableStringLength?.div(5)?: fail("nullableStringLength is null!")
    println(s)
}