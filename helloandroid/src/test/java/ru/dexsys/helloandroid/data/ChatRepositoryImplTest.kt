package ru.dexsys.helloandroid.data

import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.joda.time.DateTime
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.spy
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.stub
import org.mockito.kotlin.verify
import ru.dexsys.helloandroid.data.datasources.ChatDBDataSource
import ru.dexsys.helloandroid.data.datasources.ChatLocalDataSourceImpl
import ru.dexsys.helloandroid.data.datasources.ChatRemoteDataSource
import ru.dexsys.helloandroid.domain.vo.ChatMessageVO


class ChatRepositoryImplTest {
    //newSingleThreadContext создает контекст выполнения корутины с использованием одного потока
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    //mock создает фэйковый объект данного класса или интерфейса.
    private val chatRemoteDataSource = mock(ChatRemoteDataSource::class.java)
    private val chatDBDataSource = mock(ChatDBDataSource::class.java)

    //spy создает реальный объект, который позволяет использовать фичи mockito
    private val chatLocalDataSource = spy(ChatLocalDataSourceImpl())

    private var date = DateTime.parse("2021-01-30T13:40:45")

    private val repository = ChatRepositoryImpl(
        chatLocalDataSource = chatLocalDataSource,
        chatRemoteDataSource = chatRemoteDataSource,
        chatDBDataSource = chatDBDataSource
    )

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        chatRemoteDataSource.stub {
            // onBlocking обеспечит запуск новой корутины и блокирующем потоке до её завершения
            onBlocking { getMessages() }.doReturn(
                listOf(
                    ChatMessageVO(
                        id = "0",
                        text = "message from REST API!",
                        isIncomeMessage = true,
                        date = date
                    )
                )
            )
        }
        chatDBDataSource.stub {
            onBlocking { getMessages() }.doReturn(
                listOf(
                    ChatMessageVO(
                        id = "1",
                        text = "message from DB!",
                        isIncomeMessage = true,
                        date = date
                    )
                )
            )
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun dirtyCacheIsCleared() {
        runBlocking {
            launch(Dispatchers.Main) {
                repository.getMessages().collect {
                    assertEquals("message from REST API!", it[0].text)
                }
                repository.getMessages().collect {
                    assertThat("message from DB!", `is`(it[0].text))
                }
                verify(chatRemoteDataSource).getMessages()
                verify(chatDBDataSource).getMessages()
            }
        }
    }
}