package ru.dexsys.helloandroid

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

private const val POSITION_PUBLIC = 0
private const val POSITION_PRIVATE = 1

class ProfileAdapter(fragment: Fragment, private val titles: Array<String>) :
//Вместо View для наполнения нашего ViewPager будем использовать фрагменты
    FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return titles.size
    }

    //Создаем фрагмент для заданной позиции
    override fun createFragment(position: Int): Fragment =
        when (position) {
            POSITION_PUBLIC -> PublicInfoFragment.newInstance("", "")
            POSITION_PRIVATE -> PrivateInfoFragment.newInstance("", "")
            else -> throw IllegalArgumentException("wrong position in ru.dexsys.helloandroid.ProfileAdapter.createFragment method")
        }
}
