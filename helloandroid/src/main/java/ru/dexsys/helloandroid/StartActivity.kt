package ru.dexsys.helloandroid

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import ru.dexsys.helloandroid.databinding.ActivityStartBinding
import javax.inject.Inject

// AppCompatActivity - Базовый класс для Activity, в которых планируется использовать некоторые из новых функций
// платформы на старых устройствах Android. Наследуется от FragmentActivity - Специальная реализация Activity,
// предназначенная для работы с фрагментами
class StartActivity : AppCompatActivity(), HasAndroidInjector {
    // Поля для хранения объекта Binding
    private var _binding: ActivityStartBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var mvvmActions: MVVMActions

    // Вызывается один раз с момента создания Activity до её уничтожения
    // Используйте для стартовой настройки Activity и для инициализации макета
    override fun onCreate(savedInstanceState: Bundle?) {
        _binding = ActivityStartBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        val view = binding.root
        setContentView(view)
        initListeners()
    }

    override fun androidInjector(): AndroidInjector<Any> = fragmentInjector

    private fun initListeners() {
        // Здесь мы используя ViewBinding, получаем ссылку на нашу кнопку и присваиваем ей
        // шлушатель нажитий
        binding.buttonToMainActivity.setOnClickListener { navigateToMainActivity() }
        binding.buttonToBottomNavigationActivity.setOnClickListener { navigateToBottomNavigationActivity() }
        binding.buttonToMVVMActivity.setOnClickListener {
            mvvmActions.navigateToImportantActivity(
                this
            )
        }
    }

    private fun navigateToMainActivity() {
        //Intent - это объект обмена сообщениями, который вы можете использовать для запроса действия
        // от другого компонента приложения. Существует три основных варианта использования интентов:
        // -запуск Activity
        // -запуск Service
        // -доставка Broadcast-сообщений
        // Интенты бывают явными и неявными
        // В данном случае мы создаем интент для станта Activity
        val intent = Intent(this, MainActivity::class.java).apply {
            putExtra(Intent.EXTRA_TEXT, "Вы перешли сюда со стартового экрана")
        }
        startActivity(intent)
    }

    private fun navigateToBottomNavigationActivity() {
        val intent = Intent(this, BottomNavigationActivity::class.java)
        startActivity(intent)
    }

    // Вызов onStart() делает Activity видимым для пользователя, поскольку приложение готовится
    // к тому, чтобы Activity перешло на передний план. Например, в этом блоке вы можете делать
    // инициализацию кода связанного с UI
    override fun onStart() {
        super.onStart()
    }

    // Это состояние, в котором приложение взаимодействует с пользователем. Приложение остается
    // в этом состоянии до тех пор пока не потеряет фокус. Фокус работает даже в многооконном режиме
    override fun onResume() {
        super.onResume()
    }

    // Система вызывает этот метод как первое указание на то, что пользователь покидает вашу
    // Activity (хотя это не всегда означает, что Activity уничтожается);
    // Он указывает, что Activity больше не находится на переднем плане
    // (хотя она может быть видна, если пользователь находится в многооконном режиме).
    // Используется, чтобы приостановить или настроить операции, которые не должны продолжаться,
    // пока Activity находится в состоянии Paused
    override fun onPause() {
        super.onPause()
    }

    // Когда ваша Activity больше не отображается для пользователя, она переходит в состояние
    // Stopped, и система вызывает callback onStop(). Это может произойти, например,
    // когда другое приложение занимает весь экран. Система также может вызвать onStop (),
    // когда действие завершено и вот-вот будет завершено
    override fun onStop() {
        super.onStop()
    }

    //onDestroy() вызывается перед уничтожением Activity. Система вызывает этот callback
    // по одной из следующих причин:
    //
    // - Activity завершается (из-за того, что пользователь полностью закрыл Activity или из-за того,
    // что для Activity был вызван метод finish ()), или
    // - система временно прерывает Activity из-за изменения конфигурации (например, вращения
    // устройства или многооконного режима)
    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }

    // Вызывается, когда Activity может быть временно уничтожена, сохраните здесь состояние экземпляра
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    // Этот callback вызывается только тогда, когда есть сохраненный экземпляр, который ранее был
    // сохранен с помощью onSaveInstanceState(). Мы можем восстаносить и какое-то состояние в onCreate(),
    // но onRestoreInstanceState вызывается после onStart() и savedInstanceState гарантированно не будет
    // равен null.

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
    }
}