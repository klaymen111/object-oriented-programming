package ru.dexsys.helloandroid

import android.content.Context
import android.content.Intent
import javax.inject.Inject

// api модуля HelloAndroid
interface HelloAndroidApi {
    fun navigateToHelloAndroidActivity(context: Context)
}

class HelloAndroidApiImpl @Inject constructor() : HelloAndroidApi {
    override fun navigateToHelloAndroidActivity(context: Context) {
        context.startActivity(Intent(context, MainActivity::class.java))
    }
}