package ru.dexsys.helloandroid

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import ru.dexsys.helloandroid.databinding.ActivityBottomNavigationBinding
import ru.dexsys.helloandroid.presentation.ChatFragment
import ru.dexsys.helloandroid.presentation.SettingsFragment
import ru.dexsys.helloandroid.utils.analytics.AnalyticsManager
import ru.dexsys.helloandroid.utils.analytics.AnalyticsTags
import javax.inject.Inject


class BottomNavigationActivity : AppCompatActivity(), HasAndroidInjector {
    private var _binding: ActivityBottomNavigationBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var analytics: AnalyticsManager

    override fun onCreate(savedInstanceState: Bundle?) {
        _binding = ActivityBottomNavigationBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        val view = binding.root
        setContentView(view)
        //Инициализация BottomNavigationView
        initBottomNavigation()
    }

    override fun androidInjector(): AndroidInjector<Any> = fragmentInjector

    private fun initBottomNavigation() {
        //Inflate пунктов меню из ресурса меню
        binding.viewNavigation.inflateMenu(R.menu.menu_navigation)
        val fragmentManager = supportFragmentManager
        //Присваиваем слушатель выбора элемента нижнего навигатора
        binding.viewNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.actionChat -> {
                    analytics.sendCustomEvent(AnalyticsTags.OPEN_CHAT_SCREEN)
                    ChatFragment.newInstance("", "")
                }
                R.id.actionProfile -> {
                    analytics.sendCustomEvent(AnalyticsTags.OPEN_PROFILE_SCREEN)
                    ProfileFragment.newInstance("", "")
                }
                R.id.actionSettings -> {
                    analytics.sendCustomEvent(AnalyticsTags.OPEN_SETTINGS_SCREEN)
                    SettingsFragment.newInstance("", "")
                }
                else -> throw IllegalArgumentException("Illegal menu item")
            }.also {
                val transaction: FragmentTransaction = fragmentManager.beginTransaction()
                transaction.setCustomAnimations(
                    R.anim.slide_in_from_right,
                    R.anim.slide_out_to_left
                )
                //Делаем транзакцию выбранного фрагмента, не добавляя его в бэкстэк
                transaction.replace(R.id.container, it)
                transaction.commit()
            }
            //В нашем случае всегда возвращаем true, потому что у нас нет заблокированных элементов меню
            true
        }
        //По-умолчанию выбран экран "Чат"
        binding.viewNavigation.selectedItemId = R.id.actionChat
    }
}