package ru.dexsys.helloandroid

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerApplication
import ru.dexsys.helloandroid.di.DaggerModuleGraph
import ru.dexsys.helloandroid.di.Injectable
import javax.inject.Inject

//Компонент это сущность, которая хранит имплементацию api и инициализирует граф зависимостей
class HelloAndroidComponent(private val dependencies: HelloAndroidDependencies) {

    @Inject
    lateinit var api: HelloAndroidApi

    //Граф нашего стартового модуля в данном случае инициализируется из application, т.к. того требует dagger android
    fun init(app: DaggerApplication): AndroidInjector<out DaggerApplication> {
        val injector = DaggerModuleGraph.builder()
            // Добавляем в граф наш MainApplication
            .plus(app)
            .plus(dependencies)
            // Получаем экземпляр нашего графа
            .create(app) as DaggerModuleGraph
        // Делаем иньекцию нашего графа в MainApplication
        injector.inject(this)
        // Регистрируем слушатель жизненного цикла активности
        app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                // Внедряем граф в только что открытую активность
                handleActivity(activity)
            }

            override fun onActivityStarted(activity: Activity) {}
            override fun onActivityResumed(activity: Activity) {}
            override fun onActivityPaused(activity: Activity) {}
            override fun onActivityStopped(activity: Activity) {}
            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}
            override fun onActivityDestroyed(activity: Activity) {}
        })
        return injector
    }

    private fun handleActivity(activity: Activity) {
        if (activity is FragmentActivity && activity is HasAndroidInjector) {
            AndroidInjection.inject(activity)
            activity.supportFragmentManager
                // Регистрируем слушатель жизненного цикла фрагмента
                .registerFragmentLifecycleCallbacks(
                    object : FragmentManager.FragmentLifecycleCallbacks() {
                        override fun onFragmentCreated(
                            manager: FragmentManager,
                            fragment: Fragment,
                            savedInstanceState: Bundle?
                        ) {
                            // Внедряем граф в только что открытый фрагмент
                            if (fragment is Injectable) {
                                AndroidSupportInjection.inject(fragment)
                            }
                        }
                    }, true
                )
        }
    }
}

//Интерыейс объявляющий внешние зависимости модуля
interface HelloAndroidDependencies {
    fun getMVVMActions(): MVVMActions
}

interface MVVMActions {
    //Навигацию на ImportantActivity реализует модуль MVVM
    fun navigateToImportantActivity(context: Context)
}
