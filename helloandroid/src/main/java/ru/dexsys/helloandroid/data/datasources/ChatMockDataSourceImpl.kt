package ru.dexsys.helloandroid.data.datasources

import org.joda.time.DateTime
import ru.dexsys.helloandroid.domain.vo.ChatMessageVO
import javax.inject.Inject

interface ChatLocalDataSource {
    fun getMessages(): List<ChatMessageVO>
    fun saveMessages(messages: List<ChatMessageVO>)
}

open class ChatLocalDataSourceImpl @Inject constructor() : ChatLocalDataSource {
    private var _messages = emptyList<ChatMessageVO>()

    override fun getMessages(): List<ChatMessageVO> = _messages

    override fun saveMessages(messages: List<ChatMessageVO>) {
        _messages = messages
    }

}

//Источник мокированных данных для чата
class ChatMockDataSourceImpl @Inject constructor() : ChatLocalDataSource {
    val date = DateTime()
    val id = "0"

    override fun getMessages(): List<ChatMessageVO> = listOf(
        ChatMessageVO(id = id, text = "Привет!", isIncomeMessage = true, date = date),
        ChatMessageVO(id = id, text = "Как дела?", isIncomeMessage = true, date = date),
        ChatMessageVO(
            id = id,
            text = "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium " +
                    "doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore " +
                    "veritatis et quasi architecto beatae vitae dicta sunt, explicabo",
            isIncomeMessage = true,
            date = date
        ),
        ChatMessageVO(
            id = id,
            text = "Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, " +
                    "sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt," +
                    " neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, " +
                    "adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et " +
                    "dolore magnam aliquam quaerat voluptatem",
            isIncomeMessage = true,
            date = date
        ),
        ChatMessageVO(
            id = id,
            text = "Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit " +
                    "laboriosam, nisi ut aliquid ex ea commodi consequatur?",
            isIncomeMessage = true,
            date = date
        ),
        ChatMessageVO(
            id = id,
            text = "Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil " +
                    "molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla " +
                    "pariatur?",
            isIncomeMessage = true,
            date = date
        )
    )

    override fun saveMessages(messages: List<ChatMessageVO>) {
    }
}