package ru.dexsys.helloandroid.data.datasources

import ru.dexsys.helloandroid.data.NetworkApi
import ru.dexsys.helloandroid.data.converters.MessagesConverter
import ru.dexsys.helloandroid.domain.vo.ChatMessageVO
import javax.inject.Inject

interface ChatRemoteDataSource {
    suspend fun getMessages(): List<ChatMessageVO>
}

class ChatRemoteDataSourceImpl @Inject constructor(
    private val networkApi: NetworkApi,
    private val messagesConverter: MessagesConverter
) :
    ChatRemoteDataSource {
    override suspend fun getMessages(): List<ChatMessageVO> =
        messagesConverter.convert(networkApi.getMessages())
}
