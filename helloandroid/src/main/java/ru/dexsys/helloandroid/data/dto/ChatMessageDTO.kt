package ru.dexsys.helloandroid.data.dto

import com.google.gson.annotations.SerializedName
import java.util.*

data class ChatMessageDTO(
    @SerializedName("id")
    val id: String,
    @SerializedName("isIncome")
    val isIncome: Boolean,
    @SerializedName("text")
    val text: String,
    @SerializedName("date")
    val date: Date
)
