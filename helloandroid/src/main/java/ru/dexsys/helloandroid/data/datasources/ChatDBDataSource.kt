package ru.dexsys.helloandroid.data.datasources

import ru.dexsys.helloandroid.data.ChatDataBase
import ru.dexsys.helloandroid.domain.vo.ChatMessageVO
import javax.inject.Inject

interface ChatDBDataSource {
    suspend fun getMessages(): List<ChatMessageVO>

    suspend fun saveMessages(messages: List<ChatMessageVO>)
}

class ChatDBDataSourceImpl @Inject constructor(private val database: ChatDataBase) :
    ChatDBDataSource {
    private val dao by lazy { database.chatMessagesDao() }

    override suspend fun getMessages(): List<ChatMessageVO> = dao.getAllChatMessages()

    override suspend fun saveMessages(messages: List<ChatMessageVO>) {
        dao.insertAll(messages)
    }
}