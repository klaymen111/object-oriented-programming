package ru.dexsys.helloandroid.data.converters

import org.joda.time.DateTime
import ru.dexsys.helloandroid.data.dto.ChatMessageDTO
import ru.dexsys.helloandroid.domain.vo.ChatMessageVO
import javax.inject.Inject

interface MessagesConverter {
    fun convert(messages: List<ChatMessageDTO>): List<ChatMessageVO>
}

class MessagesConverterImpl @Inject constructor() : MessagesConverter {
    override fun convert(messages: List<ChatMessageDTO>): List<ChatMessageVO> =
        messages.map {
            ChatMessageVO(
                id = it.id,
                text = it.text,
                isIncomeMessage = it.isIncome,
                date = DateTime(it.date)
            )
        }
}
