package ru.dexsys.helloandroid.data

import retrofit2.http.GET
import ru.dexsys.helloandroid.data.dto.ChatMessageDTO

interface NetworkApi {

    @GET("messages")
    suspend fun getMessages(): List<ChatMessageDTO>

//Retrofit обеспечивает удобную работу с query params (@QueryMap, @Query), с индексами или
// динамическими путями (@Path), с телами POST запросов (@Body), с заголовками (@Header),
// multi-part form запросами и многое другое

//    @GET("anotherApi/someRestMethod/{id}")
//    suspend fun someRestMethod(@Path("id") id: String): List<SomeDTO>
}

