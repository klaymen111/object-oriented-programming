package ru.dexsys.helloandroid.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.dexsys.helloandroid.data.converters.DateTimeTypeConverter
import ru.dexsys.helloandroid.data.dao.ChatMessageDAO
import ru.dexsys.helloandroid.domain.vo.ChatMessageVO

const val CHAT_DATA_BASE_VERSION = 1

@Database(entities = [ChatMessageVO::class], version = CHAT_DATA_BASE_VERSION)
@TypeConverters(DateTimeTypeConverter::class)
abstract class ChatDataBase : RoomDatabase() {
    abstract fun chatMessagesDao(): ChatMessageDAO
}