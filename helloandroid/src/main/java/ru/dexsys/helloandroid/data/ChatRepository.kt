package ru.dexsys.helloandroid.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import ru.dexsys.helloandroid.data.datasources.ChatDBDataSource
import ru.dexsys.helloandroid.data.datasources.ChatLocalDataSource
import ru.dexsys.helloandroid.data.datasources.ChatRemoteDataSource
import ru.dexsys.helloandroid.domain.vo.ChatMessageVO
import javax.inject.Inject

// Репозиторий отвечает только за предоставление бизнес-данных из разных источников (удаленные,
// локальные, мокированные), за логику их кэширования и последующего использования
interface ChatRepository {
    suspend fun getMessages(): Flow<List<ChatMessageVO>>
}

class ChatRepositoryImpl @Inject constructor(
    private val chatLocalDataSource: ChatLocalDataSource,
    private val chatRemoteDataSource: ChatRemoteDataSource,
    private val chatDBDataSource: ChatDBDataSource
) : ChatRepository {
    private var cacheIsDirty = true

    override suspend fun getMessages(): Flow<List<ChatMessageVO>> {
        return flowOf(
            if (cacheIsDirty) {
                chatRemoteDataSource.getMessages().also {
                    chatDBDataSource.saveMessages(it)
                    cacheIsDirty = false
                }
            } else {
                chatDBDataSource.getMessages()
            }).flowOn(Dispatchers.IO)
    }
}

