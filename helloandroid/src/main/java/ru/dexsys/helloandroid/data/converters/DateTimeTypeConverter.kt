package ru.dexsys.helloandroid.data.converters

import androidx.room.TypeConverter
import org.joda.time.DateTime

class DateTimeTypeConverter {
    @TypeConverter
    fun fromDateTime(date: DateTime) = date.toString()

    @TypeConverter
    fun toDateTime(string: String) = DateTime(string)
}