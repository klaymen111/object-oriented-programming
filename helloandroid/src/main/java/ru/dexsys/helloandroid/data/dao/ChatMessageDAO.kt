package ru.dexsys.helloandroid.data.dao

import androidx.room.*
import ru.dexsys.helloandroid.domain.vo.ChatMessageVO

@Dao
interface ChatMessageDAO {
    // Добавление ChatMessageVO в бд
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(messages: List<ChatMessageVO>)

    // Удаление ChatMessageVO из бд
    @Delete
    suspend fun delete(messages: List<ChatMessageVO>)

    // Получение всех ChatMessageVO из бд
    @Query("SELECT * FROM chat_message")
    suspend fun getAllChatMessages(): List<ChatMessageVO>

    // Получение всех ChatMessageVO из бд по признаку входящие/исходящие
    @Query("SELECT * FROM chat_message WHERE is_income_message LIKE :isIncome")
    suspend fun getChatMessagesByType(isIncome: Boolean): List<ChatMessageVO>
}