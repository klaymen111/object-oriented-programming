package ru.dexsys.helloandroid.navigation

import androidx.core.os.bundleOf
import com.github.terrakok.cicerone.androidx.FragmentScreen
import ru.dexsys.helloandroid.ARG_RESULT
import ru.dexsys.helloandroid.AwesomeFragment
import ru.dexsys.helloandroid.ViewAndViewGroupFragment

object MainScreens {
    fun awesome(message: String, isChildFragment: Boolean = false) =
        FragmentScreen { AwesomeFragment.newInstance(message, isChildFragment) }

    fun viewAndViewGroup(resultKey: String) = FragmentScreen {
        ViewAndViewGroupFragment().apply {
            arguments = bundleOf(
                ARG_RESULT to resultKey
            )
        }
    }
}