package ru.dexsys.helloandroid.navigation.routers

import com.github.terrakok.cicerone.Router
import com.github.terrakok.cicerone.androidx.FragmentScreen
import ru.dexsys.helloandroid.navigation.commands.PushForward
import ru.dexsys.helloandroid.navigation.commands.ReplaceOver

/**
 * Роутер, реализующий кастомные команды.
 */
class HelloRouter : Router() {

    fun pushTo(screen: FragmentScreen) {
        executeCommands(PushForward(screen))
    }

    fun replaceOver(screen: FragmentScreen) {
        executeCommands(ReplaceOver(screen))
    }
}