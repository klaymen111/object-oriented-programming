package ru.dexsys.helloandroid.navigation.commands

import com.github.terrakok.cicerone.Command
import com.github.terrakok.cicerone.androidx.FragmentScreen

data class PushForward(val screen: FragmentScreen) : Command