package ru.dexsys.helloandroid.navigation

import androidx.annotation.IdRes
import androidx.fragment.app.*
import com.github.terrakok.cicerone.Command
import com.github.terrakok.cicerone.androidx.AppNavigator
import com.github.terrakok.cicerone.androidx.FragmentScreen
import ru.dexsys.helloandroid.R
import ru.dexsys.helloandroid.navigation.commands.PushForward
import ru.dexsys.helloandroid.navigation.commands.ReplaceOver

class HelloNavigator(
    activity: FragmentActivity,
    fragmentManager: FragmentManager,
    @IdRes containerId: Int
) : AppNavigator(activity, containerId, fragmentManager) {

    override fun setupFragmentTransaction(
        screen: FragmentScreen,
        fragmentTransaction: FragmentTransaction,
        currentFragment: Fragment?,
        nextFragment: Fragment
    ) {
        fragmentTransaction.setCustomAnimations(
            R.anim.slide_in_from_left,
            R.anim.slide_in_from_right,
            R.anim.slide_in_from_right,
            R.anim.slide_in_from_left
        )
    }

    public override fun applyCommand(command: Command) {
        when (command) {
            is ReplaceOver -> {
                back()
                applyCommand(PushForward(command.screen))
            }
            is PushForward -> pushFragment(command.screen)
            else -> super.applyCommand(command)
        }
    }

    private fun pushFragment(screen: FragmentScreen) {
        val fragment = screen.createFragment(FragmentFactory())
        val transaction = fragmentManager.beginTransaction()
        transaction.add(containerId, fragment, screen.screenKey)
        transaction.addToBackStack(screen.screenKey)
        localStackCopy.add(screen.screenKey)
        transaction.commit()
    }
}