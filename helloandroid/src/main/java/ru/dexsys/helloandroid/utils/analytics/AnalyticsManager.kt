package ru.dexsys.helloandroid.utils.analytics

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

interface AnalyticsManager {
    fun sendCustomEvent(tag: String)
}

class AnalyticsManagerImpl @Inject constructor(context: Context) : AnalyticsManager {
    private var analytics: FirebaseAnalytics = FirebaseAnalytics.getInstance(context)

    override fun sendCustomEvent(tag: String) {
        with(analytics) {
            logEvent(tag, Bundle())
        }
    }
}