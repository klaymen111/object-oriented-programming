package ru.dexsys.helloandroid.utils.analytics

object AnalyticsTags {
        const val OPEN_CHAT_SCREEN = "OPEN_CHAT_SCREEN"
        const val OPEN_PROFILE_SCREEN = "OPEN_PROFILE_SCREEN"
        const val OPEN_SETTINGS_SCREEN = "OPEN_SETTINGS_SCREEN"
}