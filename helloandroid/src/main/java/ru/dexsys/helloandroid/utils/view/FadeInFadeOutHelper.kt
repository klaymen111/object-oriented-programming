package ru.dexsys.helloandroid.utils.view

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.view.isVisible

//Хелпер для быстрого применения анимации исчезновения и появления.
private const val DEFAULT_DURATION = 300L

class FadeInFadeOutHelper(val view: View) {

    private var isFadeOutRunning: Boolean = false
    var animatorSet = AnimatorSet()

    fun fadeIn(duration: Long = DEFAULT_DURATION, maxAlpha: Float = 1f) {
        if (!view.isVisible || isFadeOutRunning) {
            isFadeOutRunning = false
            animatorSet.removeAllListeners()
            if (animatorSet.isRunning) {
                animatorSet.cancel()
            }
            val startAlpha: Float = view.alpha
            val endAlpha: Float = maxAlpha
            animatorSet = AnimatorSet()
            animatorSet.duration = duration
            animatorSet.interpolator = AccelerateDecelerateInterpolator()
            animatorSet.play(createFadeAnimation(startAlpha, endAlpha))
            animatorSet.start()
        }
    }

    fun fadeOut(duration: Long = DEFAULT_DURATION, minAlpha: Float = 0f) {
        if (view.isVisible && !isFadeOutRunning) {
            isFadeOutRunning = true
            animatorSet.removeAllListeners()
            if (animatorSet.isRunning) {
                animatorSet.cancel()
            }
            val startAlpha: Float = view.alpha
            val endAlpha: Float = minAlpha
            animatorSet = AnimatorSet()
            animatorSet.duration = duration
            animatorSet.play(createFadeAnimation(startAlpha, endAlpha))
            animatorSet.start()
        }
    }

    private fun createFadeAnimation(startAlpha: Float, endAlpha: Float): Animator? {
        val animator = ObjectAnimator.ofFloat(view, View.ALPHA, startAlpha, endAlpha)
        if (endAlpha > 0f) view.isVisible = true
        animator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator) {
            }

            override fun onAnimationCancel(p0: Animator) {
            }

            override fun onAnimationStart(p0: Animator) {
            }

            override fun onAnimationEnd(p0: Animator) {
                if (endAlpha == 0f) {
                    view.visibility = View.INVISIBLE
                    isFadeOutRunning = false
                }
            }
        })
        return animator
    }

    fun endAnimation() {
        if (animatorSet.isRunning) {
            animatorSet.end()
        }
    }

    fun cancelAnimation() {
        if (!animatorSet.listeners.isNullOrEmpty())
            animatorSet.cancel()
    }
}