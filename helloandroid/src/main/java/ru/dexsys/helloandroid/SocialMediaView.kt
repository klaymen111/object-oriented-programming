package ru.dexsys.helloandroid

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.LinearLayoutCompat

/**
 * Created by Grigorii Shadrin on 19.04.2021.
 * <p>
 * View для работы с социальными сетями
 */
//Кастомная View
class SocialMediaView
//Аннотация @JvmOverloads позволяет перегрузить конструктор автоматически
//Обязательно нужно использовтаь дефолтные значения
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    LinearLayoutCompat(context, attrs, defStyleAttr) {

    private var iconMargin: Int = 0
    var socialNetworkClickListener: SocialNetworkClickListener? = null

    init {
        orientation = HORIZONTAL
        initAttrs(attrs)
    }

    private fun initAttrs(attrs: AttributeSet?) {
        if (attrs != null) {
            val array = context.obtainStyledAttributes(attrs, R.styleable.SocialMediaView)
            try {
                iconMargin =
                    array.getDimensionPixelSize(R.styleable.SocialMediaView_iconMargin, 10.px)
            } catch (ignored: NullPointerException) {
            } finally {
                array.recycle()
            }
        }
    }

    var items: List<SocialNetworkItem>? = null
        set(value) {
            field = value
            removeAllViews()
            items?.let {
                for (item in it) {
                    val view = AppCompatImageView(context)
                    view.layoutParams = LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    ).apply {
                        rightMargin = iconMargin
                        topMargin = 9.px
                        bottomMargin = 9.px
                    }
                    view.setImageResource(item.iconRes)
                    view.isClickable = true
                    view.isFocusable = true
                    if (socialNetworkClickListener != null) {
                        view.setOnClickListener {
                            socialNetworkClickListener?.onSocialNetworkItemClick(
                                item
                            )
                        }
                    }
                    addView(view)
                }
            }
            //инвалидация View, благодаря которой в будущем будет вызван метотод onDraw
            invalidate()
        }

    // Вызывается для определения размеров
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    // Вызывается для расположения элемента внутри контейнера
    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
    }

    // Вызывается для отрисовки
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
    }
}

interface SocialNetworkClickListener {
    fun onSocialNetworkItemClick(item: SocialNetworkItem)
}

data class SocialNetworkItem(
    @DrawableRes val iconRes: Int,
    val url: String
)