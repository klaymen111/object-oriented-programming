package ru.dexsys.helloandroid

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import ru.dexsys.helloandroid.databinding.FragmentViewAndViewGroupBinding
import ru.dexsys.helloandroid.di.Injectable
import ru.dexsys.helloandroid.navigation.routers.HelloRouter
import ru.dexsys.helloandroid.utils.view.FadeInFadeOutHelper
import javax.inject.Inject

const val ARG_RESULT = "arg_result"

class ViewAndViewGroupFragment : Fragment(), Injectable {
    private var _binding: FragmentViewAndViewGroupBinding? = null
    private val binding get() = _binding!!

    private var resultKey: String? = null

    private val programmaticallyAddedText = "Programmatically added"
    private val fadeHelper by lazy { FadeInFadeOutHelper(binding.imageNinja) }

    @Inject
    lateinit var router: HelloRouter

    companion object {
        @JvmStatic
        fun newInstance(resultKey: String) = ViewAndViewGroupFragment().apply {
            arguments = bundleOf(ARG_RESULT to resultKey)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            resultKey = it.getString(ARG_RESULT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentViewAndViewGroupBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        initUi()
    }

    private fun initListeners() {
        binding.button.setOnClickListener {
            val typedText = binding.editText.text.toString()
            resultKey?.let {
                router.sendResult(it, typedText)
                router.exit()
            }
        }
        binding.buttonFadeIn.setOnClickListener {
            fadeHelper.fadeIn(1000L)
        }
        binding.buttonFadeOut.setOnClickListener {
            fadeHelper.fadeOut(1000L)
        }
        binding.buttonVisible.setOnClickListener { binding.imageNinja.isVisible = true }
        binding.buttonGone.setOnClickListener { binding.imageNinja.isVisible = false }
    }

    private fun initUi() {
        binding.layoutLinear.addView(TextView(context).apply {
            layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
                .also { it.setMargins(20, 20, 20, 20) }
            text = programmaticallyAddedText
            textSize = 20f
        })
        binding.viewSocialMedia.items = listOf(
            SocialNetworkItem(R.drawable.ic_telegram, ""),
            SocialNetworkItem(R.drawable.ic_vk, ""),
            SocialNetworkItem(R.drawable.ic_youtube, "")
        )
    }
}