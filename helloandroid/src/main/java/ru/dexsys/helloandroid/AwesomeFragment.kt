package ru.dexsys.helloandroid

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import ru.dexsys.helloandroid.databinding.FragmentAwesomeBinding
import ru.dexsys.helloandroid.di.Injectable
import ru.dexsys.helloandroid.navigation.MainScreens
import ru.dexsys.helloandroid.navigation.routers.HelloRouter
import javax.inject.Inject
import kotlin.random.Random

const val ARG_MESSAGE = "arg_message"
const val ARG_CHILD = "arg_child"

class AwesomeFragment : Fragment(), Injectable {
    private var _binding: FragmentAwesomeBinding? = null
    private val binding get() = _binding!!
    private var counter = 0
    private var spacer = ""

    private var message: String? = null
    private var isChildFragment: Boolean = false

    @Inject
    lateinit var router: HelloRouter

    companion object {
        @JvmStatic
        fun newInstance(message: String, isChildFragment: Boolean) =
            AwesomeFragment().apply {
                arguments = bundleOf(
                    ARG_MESSAGE to message,
                    ARG_CHILD to isChildFragment
                )
            }
    }

    //Вызывается, когда Fragment связывается с Activity в FragmentManager
    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    // В этом методе можно сделать работу, не связанную с интерфейсом. Например, подготовить адаптер,
    // восстановить некоторые данные после дестроя, инициализировать аргументы
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            message = it.getString(ARG_MESSAGE)
            isChildFragment = it.getBoolean(ARG_CHILD)
        }
    }

    //Здесь происходит инициализация макета
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAwesomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    // Это подходящее место для настройки начального состояния ваших View
    // и для настройки адаптеров RecyclerView или ViewPager2
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        updateUi()
    }

    private fun initListeners() {
        binding.buttonChildFragment.setOnClickListener {
            val screen = MainScreens.awesome(message = spacer + counter, isChildFragment = true)
            spacer += "     "
            counter += 1
            router.pushTo(screen)
        }
    }

    private fun updateUi() {
        binding.textMessage.text = message
        if (!isChildFragment) {
            binding.buttonChildFragment.isVisible = true
            val color: Int =
                Color.argb(255, Random.nextInt(256), Random.nextInt(256), Random.nextInt(256))
            binding.root.setBackgroundColor(color)
        } else {
            binding.textMessage.updateLayoutParams<FrameLayout.LayoutParams> {
                topMargin = 50.px
            }
        }
    }

    //Состояние фрагмента было восстановлено
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    //Вызывается, когда View удаляются
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    //Окончательное уничтожение фрагмена
    override fun onDestroy() {
        super.onDestroy()
    }

    //Вызывается, когда Fragment отсоединен от Activity
    override fun onDetach() {
        super.onDetach()
    }
}