package ru.dexsys.helloandroid

import kotlin.properties.Delegates

//ПАТТЕРНЫ ПРОЕКТИРОВАНИЯ

// Паттерны (или шаблоны) проектирования описывают типичные способы решения часто встречающихся
// проблем при проектировании программ. Не путать с алгоритмами

//Для чего знать паттерны?

//- Проверенные решения. Вы тратите меньше времени, используя готовые решения, вместо повторного
// изобретения велосипеда. До некоторых решений вы смогли бы додуматься и сами, но многие могут быть
// для вас открытием.

//- Стандартизация кода. Вы делаете меньше просчётов при проектировании, используя типовые
// унифицированные решения, так как все скрытые проблемы в них уже давно найдены.

//- Общий программистский словарь. Вы произносите название паттерна, вместо того, чтобы час объяснять
// другим программистам, какой крутой дизайн вы придумали и какие классы для этого нужны.

//Типы паттернов:

//Порождающие паттерны беспокоятся о гибком создании объектов без внесения в программу лишних зависимостей.
//Структурные паттерны показывают различные способы построения связей между объектами.
//Поведенческие паттерны заботятся об эффективной коммуникации между объектами.

//1. Одиночка — это порождающий паттерн проектирования, который гарантирует, что у класса есть только
// один экземпляр, и предоставляет к нему глобальную точку доступа.
object JustSingleton {
    val value: String = "Just a value"
}

//2. Декоратор — это структурный паттерн проектирования, который позволяет динамически добавлять
// объектам новую функциональность, оборачивая их в полезные «обёртки».
class HappyMap<K, V>(val mutableMap: MutableMap<K, V> = mutableMapOf()) :
    MutableMap<K, V> by mutableMap {
    override fun put(key: K, value: V): V? {
        return mutableMap.put(key, value).apply {
            if (this == null) {
                println("Yay! $key")
            }
        }
    }
}

class DecoratorUsage {
    var map = HappyMap(mutableMapOf("one" to 1, "two" to 2))

    fun useDecoration() {
        map["three"] = 3
    }
}

//3. Фабричный метод — это порождающий паттерн проектирования, который определяет общий интерфейс для
// создания объектов в суперклассе, позволяя подклассам изменять тип создаваемых объектов.
class Car private constructor(
    private val wheelsCount: Int = 4,
    private val color: String = "White",
    private val hasRoof: Boolean = true
) {
    companion object {
        fun createRollsRoycePhantom(color: String): Car {
            return Car(wheelsCount = 4, color = color, hasRoof = false)
        }

        fun createHondaGyro(color: String): Car {
            return Car(wheelsCount = 3, color = color, hasRoof = true)
        }
    }
}

class CarFactoryUsage {
    val redRollsRoycePhantom = Car.createRollsRoycePhantom("red")
    val blueHondaGyro = Car.createHondaGyro("blue")
}

//4. Стратегия — это поведенческий паттерн, выносит набор алгоритмов в собственные классы и делает
// их взаимозаменимыми

class StrategicTextProcessor(val rawText: String) {
    var processingStrategy: ((String) -> String)? = null

    fun process() = processingStrategy?.invoke(rawText) ?: rawText
}

class StrategyUsage {
    private val trimStrategy: (String) -> String = { it.trim() }
    private val antiKittyStrategy: (String) -> String = { it.replace("Kitty", "").trim() }

    private val rawText = "Hello Kitty!"
    private val processor = StrategicTextProcessor(rawText)

    fun useStrategy() {
        if (rawText.contains("Kitty")) {
            processor.processingStrategy = antiKittyStrategy
        } else {
            processor.processingStrategy = trimStrategy
        }
        processor.process()
    }
}

//5. Builder — это порождающий паттерн проектирования, который позволяет создавать сложные
// объекты пошагово. Builder даёт возможность использовать один и тот же код строительства для
// получения разных представлений объектов.

class HomeWithNamedArgs(val floorsCount: Int = 1, val hasBasement: Boolean = false, year: Int = 0)

class HomeWithBuilder private constructor(
    val floorsCount: Int,
    val hasBasement: Boolean,
    val year: Int
) {
    private constructor(builder: Builder) : this(
        builder.floorsCount,
        builder.hasBasement,
        builder.year
    )

    companion object {
        inline fun build(block: Builder.() -> Unit) = Builder().apply(block).build()
    }

    class Builder {
        var floorsCount: Int = 1
        var hasBasement: Boolean = false
        var year: Int = 0

        fun build() = HomeWithBuilder(this)
    }
}

class BuilderUsage {
    val homeWithNamedArgs = HomeWithNamedArgs(year = 1999, hasBasement = true)
    val homeWithBuilder = HomeWithBuilder.build {
        floorsCount = 16
        year = 1960
    }
}

//6. Observer — это поведенческий паттерн проектирования, который создаёт механизм подписки,
// позволяющий одним объектам следить и реагировать на события, происходящие в других объектах.
interface TextChangedObserver {
    fun onTextChanged(oldText: String, newText: String)
}

class PrintingTextChangedObserver : TextChangedObserver {
    private var text = ""

    override fun onTextChanged(oldText: String, newText: String) {
        text = "Text is changed: $oldText -> $newText"
    }
}

class ObservableText {
    private val observers = mutableListOf<TextChangedObserver>()

    fun addObserver(observer: TextChangedObserver) {
        observers.add(observer)
    }

    fun removeObserver(observer: TextChangedObserver) {
        observers.remove(observer)
    }

    var text: String by Delegates.observable("<empty>") { _, old, new ->
        observers.forEach { it.onTextChanged(old, new) }
    }
}

class ObserverUsage {
    val observableText = ObservableText().apply {
        addObserver(PrintingTextChangedObserver())
    }

    fun useObserver() {
        with(observableText)
        {
            text = "Lorem ipsum"
            text = "dolor sit amet"
        }
    }
}

//7. Dependency Injection — процесс предоставления внешней зависимости программному компоненту.
// Является специфичной формой «инверсии управления», когда она применяется к управлению зависимостями.
// В полном соответствии с принципом единственной обязанности объект отдаёт заботу о построении
// требуемых ему зависимостей внешнему, специально предназначенному для этого общему механизму.

//Существует три основных типа внедрения зависимостей:
// - constructor injection: все зависимости передаются через конструктор класса.

// - setter injection: разработчик добавляет setter-метод, с помощью которого инжектор внедряет
// зависимость

// - interface injection: зависимость предоставляет инжектору метод, с помощью которого
// инжектор передаст зависимость. Разработчики должны реализовать интерфейс, предоставляющий
// setter-метод, который принимает зависимости

interface Cpu
interface Gpu

class Desktop() {
// Где-то в нашей кодовой базе мы инстанцирует объекты, требующие этот класс
// Два варианта реализации внедрения зависимостей:
// 1. На основе setter-метода
// 2. На основе конструктора

    // На основе сеттера
    lateinit var cpu: Cpu
    lateinit var gpu: Gpu

    // На основе конструктора
    constructor(cpu: Cpu, gpu: Gpu) : this() {
        this.cpu = cpu
        this.gpu = gpu
    }
}

class DiUsage {
    // Зависимости. Обычно зависимости создаются и хранятся на уровне доступном всему приложению или
    // конкретному модулю
    val ourCpu by lazy { object : Cpu {} }
    val outGpu by lazy { object : Gpu {} }

    fun useDi() {
        var desktop = Desktop()

        //Внедряем зависимости через сеттеры
        desktop.apply {
            cpu = ourCpu
            gpu = outGpu
        }

        //Внедряем зависимости через конструктор
        desktop = Desktop(ourCpu, outGpu)
    }
}