package ru.dexsys.helloandroid.domain

import kotlinx.coroutines.flow.Flow
import ru.dexsys.helloandroid.data.ChatRepository
import ru.dexsys.helloandroid.domain.vo.ChatMessageVO
import javax.inject.Inject

interface ChatInteractor {
    suspend fun getMessagesAsync(): Flow<List<ChatMessageVO>>
}

class ChatInteractorImpl @Inject constructor(private val chatRepository: ChatRepository) :
    ChatInteractor {

    override suspend fun getMessagesAsync(): Flow<List<ChatMessageVO>> =
        chatRepository.getMessages()
}
