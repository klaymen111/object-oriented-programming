package ru.dexsys.helloandroid.domain.vo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.DateTime

@Entity(tableName = "chat_message")
data class ChatMessageVO(
    @PrimaryKey
    val id: String,
    val text: String,
    @ColumnInfo(name = "is_income_message")
    val isIncomeMessage: Boolean,
    val date: DateTime
)