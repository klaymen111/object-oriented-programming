package ru.dexsys.helloandroid.domain

import android.os.AsyncTask
import android.os.Handler
import android.os.Looper
import android.util.Log
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

interface SettingsInteractor {
    fun verySlowBusinessOperation(doOnSuccess: () -> Unit)
    fun verySlowBusinessOperationInIoThread(doOnSuccess: () -> Unit)
    fun verySlowBusinessOperationInAsyncTask(doOnSuccess: () -> Unit)
    fun verySlowBusinessOperationInCoroutineAsync(): Deferred<Unit>
    fun getFirstImportantStringAsync(): Deferred<String>
    fun getSecondImportantStringAsync(): Deferred<String>
    fun getMightyCrushingException()
    fun getCountdownFlow(): Flow<String>
}

class SettingsInteractorImpl @Inject constructor() : SettingsInteractor {
    private val interactorScope = object : CoroutineScope {
        override val coroutineContext: CoroutineContext
            get() = Dispatchers.IO
    }

    override fun verySlowBusinessOperation(doOnSuccess: () -> Unit) {
        Thread.sleep(5000)
        doOnSuccess.invoke()
    }

    override fun verySlowBusinessOperationInIoThread(doOnSuccess: () -> Unit) {
        object : Thread() {
            override fun run() {
                verySlowBusinessOperation {
                    Handler(Looper.getMainLooper()).post { doOnSuccess.invoke() }
                }
            }
        }.start()
    }

    override fun verySlowBusinessOperationInAsyncTask(doOnSuccess: () -> Unit) {
        object : AsyncTask<Unit, Unit, Unit>() {
            override fun onPreExecute() {
                super.onPreExecute()
            }

            override fun onPostExecute(result: Unit?) {
                super.onPostExecute(result)
                doOnSuccess.invoke()
            }

            override fun doInBackground(vararg params: Unit?): Unit {
                return verySlowBusinessOperation {}
            }
        }.execute()
    }

    override fun verySlowBusinessOperationInCoroutineAsync() =
        interactorScope.async { verySlowBusinessOperation {} }

    override fun getFirstImportantStringAsync(): Deferred<String> =
        interactorScope.async {
            verySlowBusinessOperation { }
            return@async "We must be called"
        }

    override fun getSecondImportantStringAsync(): Deferred<String> =
        interactorScope.async {
            verySlowBusinessOperation { }
            return@async "together"
        }

    override fun getMightyCrushingException() {
        throw Exception("Catch me if you can!")
    }

    override fun getCountdownFlow(): Flow<String> =
        flow {
            for (count in 10 downTo -1) {
                if (count < 0) {
                    throw BoomException()
                } else {
                    emit(count)
                    delay(1000L)
                }
            }
        }
            .onEach { Log.d("onEach", "${it.javaClass.simpleName} $it") }
            .map { it.toString() }
            .catch {
                if (it is BoomException) {
                    emit("Boom!")
                    delay(1000L)
                } else throw it
            }
            .onEach { Log.d("onEach", "${it.javaClass.simpleName} $it") }
}

class BoomException : Exception()
