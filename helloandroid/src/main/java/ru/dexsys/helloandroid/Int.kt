package ru.dexsys.helloandroid

import android.content.res.Resources
import android.util.TypedValue
import kotlin.math.roundToInt

val Int.px: Int
    get() {
        val metrics = Resources.getSystem().displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), metrics).roundToInt()
    }

val Int.dp: Int
    get() {
        val metrics = Resources.getSystem().displayMetrics
        return (this / (metrics.densityDpi / 160f)).roundToInt()
    }