package ru.dexsys.helloandroid

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.github.terrakok.cicerone.NavigatorHolder
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import ru.dexsys.helloandroid.databinding.ActivityMainBinding
import ru.dexsys.helloandroid.navigation.HelloNavigator
import ru.dexsys.helloandroid.navigation.MainScreens
import ru.dexsys.helloandroid.navigation.routers.HelloRouter
import javax.inject.Inject

const val RESULT_KEY: String = "result_key"

class MainActivity : AppCompatActivity(), HasAndroidInjector {
    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var router: HelloRouter

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    private val navigator by lazy { HelloNavigator(this, supportFragmentManager, R.id.container) }

    lateinit var text: String

    override fun onCreate(savedInstanceState: Bundle?) {
        readArguments()
        _binding = ActivityMainBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        val view = binding.root
        setContentView(view)
        updateUi()
        initListeners()
    }

    override fun androidInjector(): AndroidInjector<Any> = fragmentInjector

    private fun readArguments() {
        intent.extras.also { bundle ->
            bundle?.let {
                text = (it[Intent.EXTRA_TEXT] as? String) ?: ""
            }
        }
    }

    private fun updateUi() {
        title = text
    }

    private fun initListeners() {
        binding.buttonStartFragment.setOnClickListener {
            router.navigateTo(MainScreens.awesome("Этот фрагмент находится в контексте MainActivity"))
        }
        binding.buttonToViewAndViewGroup.setOnClickListener {
            router.setResultListener(RESULT_KEY) { data ->
                binding.buttonToViewAndViewGroup.text = data as String
            }
            router.navigateTo(MainScreens.viewAndViewGroup(RESULT_KEY))
        }
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }

    // Вызывается каждый раз, когда нажимается кнопка "Назад"
    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onResumeFragments() {
        navigatorHolder.setNavigator(navigator)
        super.onResumeFragments()
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }
}