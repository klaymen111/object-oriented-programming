package ru.dexsys.helloandroid.presentation.delegates

import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import ru.dexsys.helloandroid.databinding.LayoutIncomeMessageItemBinding
import ru.dexsys.helloandroid.presentation.items.ChatItem
import ru.dexsys.helloandroid.presentation.items.IncomeMessageItem

//AdapterDelegate предоставляет все необходимые методы для работы RecyclerView.Adapter
internal fun incomeMessageDelegate() =
    adapterDelegateViewBinding</*Нужно для работы getItemViewType*/IncomeMessageItem, ChatItem, LayoutIncomeMessageItemBinding>(
        { layoutInflater, root ->
            //Идентично onCreateViewHolder
            LayoutIncomeMessageItemBinding.inflate(
                layoutInflater,
                root,
                false
            )
        }) {
        //Идентично onBindViewHolder
        bind {
            itemView.tag = item
            binding.message.text = item.messageText
        }
    }