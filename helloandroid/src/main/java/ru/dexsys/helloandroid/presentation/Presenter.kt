package ru.dexsys.helloandroid.presentation

//Интерфейс, через который презентер получает события из вью
abstract class Presenter<View> {
    abstract fun init(view: View)

    abstract fun onDestroy()
}
