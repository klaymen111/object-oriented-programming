package ru.dexsys.helloandroid.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import ru.dexsys.helloandroid.databinding.FragmentChatBinding
import ru.dexsys.helloandroid.di.Injectable
import ru.dexsys.helloandroid.presentation.adapters.ChatAdapter
import ru.dexsys.helloandroid.presentation.delegates.incomeMessageDelegate
import ru.dexsys.helloandroid.presentation.items.ChatItem
import javax.inject.Inject

//Интерфейс, через который presenter будет управлять нашим представлением
interface ChatView {
    fun updateItems(items: List<ChatItem>)
}

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

//Фрагмент должен содержать в себе только логику инициализации View и слушателей
class ChatFragment : Fragment(), ChatView, Injectable {
    private var _binding: FragmentChatBinding? = null
    private val binding get() = _binding!!

    private var param1: String? = null
    private var param2: String? = null

    lateinit var adapter: ChatAdapter

    @Inject
    lateinit var presenter: Presenter<ChatView>

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ChatFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChatBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        presenter.init(this)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    private fun initRecyclerView() {
        // Вспомогательный класс, который отвечает за измерение и расположение View элементов,
        // а также за определение политики утилизиции ViewHolder
        val linearLayoutManager = LinearLayoutManager(view?.context)
        binding.recycler.layoutManager = linearLayoutManager
        // Инициализируем адаптер
        initAdapter()
    }

    private fun initAdapter() {
        //Адаптер обеспечивают привязку набора данных Item к ViewHolder
        adapter = ChatAdapter()
        adapter.addDelegate(incomeMessageDelegate())
        binding.recycler.adapter = adapter
    }

    override fun updateItems(items: List<ChatItem>) {
        adapter.clear()
        adapter.addItems(items)
        adapter.notifyDataSetChanged()
    }
}