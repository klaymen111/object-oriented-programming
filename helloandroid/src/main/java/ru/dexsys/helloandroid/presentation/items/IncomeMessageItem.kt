package ru.dexsys.helloandroid.presentation.items

data class IncomeMessageItem(val messageText: String) : ChatItem