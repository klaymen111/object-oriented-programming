package ru.dexsys.helloandroid.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import kotlinx.coroutines.*
import ru.dexsys.helloandroid.databinding.FragmentSettingsBinding
import ru.dexsys.helloandroid.di.Injectable
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

// CoroutineScope отслеживает любую корутину, созданную с помощью launch или async
// (это функции расширения в CoroutineScope)
// Вам необходимо создавать CoroutineScope всякий раз, когда вы хотите запустить и контролировать
// жизненный цикл сопрограмм в определенном слое вашего приложения
interface SettingsView : CoroutineScope {
    // Job — это управляющий корутиной элемент. Для каждой создаваемой корутины (с помощью launch
    // или async) он возвращает экземпляр Job, который однозначно идентифицирует корутину и управляет
    // ее жизненным циклом
    val job: Job

    //CoroutineContext— это набор элементов, определяющих поведение корутины. Он состоит из:
    //
    //Job — управляет жизненным циклом корутины.
    //CoroutineDispatcher — отправляет работу в соответствующий поток.
    //CoroutineName — имя корутины, полезно для отладки.
    //CoroutineExceptionHandler — обрабатывает неотловленные исключения
    override val coroutineContext: CoroutineContext

    fun setTitle(titleText: String)
    fun showLoading()
    fun hideLoading()
    fun showToast(text: String)
    fun setButtonFlowText(text: String)
}

class SettingsFragment : Fragment(), SettingsView, Injectable {
    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!

    override val job: Job = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main + CoroutineExceptionHandler { coroutineContext, throwable ->
            presenter.onThrowException(throwable)
        }

    private var param1: String? = null
    private var param2: String? = null

    @Inject
    lateinit var presenter: SettingsPresenter<SettingsView>

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SettingsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSettingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.init(this)
        launch { presenter.initSubscriptions() }
        initListeners()
    }

    private fun initListeners() {
        binding.buttonUiThreadProcessing.setOnClickListener { presenter.onButtonUiThreadProcessingClick() }
        binding.buttonIoThreadProcessing.setOnClickListener { presenter.onButtonIoThreadProcessingClick() }
        binding.buttonAsyncTaskProcessing.setOnClickListener { presenter.onButtonAsyncTaskProcessingClick() }
        binding.buttonCoroutineProcessing.setOnClickListener { launch { presenter.onButtonCoroutineProcessingClick() } }
        binding.buttonAsyncTwoParallelMethodCalls.setOnClickListener {
            launch { presenter.onButtonAsyncTwoParallelMethodCallsClick() }
        }
        binding.buttonCoroutineError.setOnClickListener { launch { presenter.onButtonCoroutineErrorClick() } }
        binding.buttonCoroutineFlow.setOnClickListener { launch { presenter.onButtonCoroutineFlowClick() } }
        binding.editTextCoroutineDebounce.addTextChangedListener {
            launch { presenter.onTextChanged(it) }
        }
    }

    override fun setTitle(titleText: String) {
        binding.textSettings.text = titleText
    }

    override fun showLoading() {
        binding.progress.isVisible = true
    }

    override fun hideLoading() {
        binding.progress.isVisible = false
    }

    override fun showToast(text: String) {
        val toast = Toast.makeText(context, text, Toast.LENGTH_LONG)
        toast.show()
    }

    override fun setButtonFlowText(text: String) {
        binding.buttonCoroutineFlow.text = text
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}