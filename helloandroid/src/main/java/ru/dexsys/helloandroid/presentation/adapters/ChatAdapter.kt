package ru.dexsys.helloandroid.presentation.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.AdapterDelegatesManager
import ru.dexsys.helloandroid.presentation.items.ChatItem
import java.util.*

//Наследуемся
class ChatAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val delegatesManager: AdapterDelegatesManager<List<ChatItem>> = AdapterDelegatesManager()
    private val items: MutableList<ChatItem> = ArrayList()

    // Возвращает тип View элемента в заданной позиции. Будет использовано при переиспользовании ViewHolder
    override fun getItemViewType(position: Int): Int {
        return delegatesManager.getItemViewType(items, position)
    }

    //Вызывается, когда RecyclerView требуется новый ViewHolder заданного типа для View
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegatesManager.onCreateViewHolder(parent, viewType)
    }

    // Вызывается RecyclerView для отображения данных в указанной позиции. Этот метод должен
    // обновить содержимое ViewHolder.itemView
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegatesManager.onBindViewHolder(items, position, holder)
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        delegatesManager.onViewAttachedToWindow(holder)
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        delegatesManager.onViewDetachedFromWindow(holder)
    }

    // Возвращает общее количество элементов в адаптере
    override fun getItemCount(): Int {
        return items.size
    }

    fun clear() {
        items.clear()
    }

    fun addItems(items: List<ChatItem>) {
        this.items.addAll(items)
    }

    fun addItem(index: Int, ChatItem: ChatItem) {
        if (index >= 0 && index <= items.size) {
            items.add(index, ChatItem)
        }
    }

    fun addDelegate(delegate: AdapterDelegate<List<ChatItem>>): ChatAdapter {
        delegatesManager.addDelegate(delegate)
        return this
    }

    fun addDelegates(delegates: List<AdapterDelegate<List<ChatItem>>>) {
        delegates.forEach {
            addDelegate(it)
        }
    }
}