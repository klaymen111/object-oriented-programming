package ru.dexsys.helloandroid.presentation

import android.text.Editable
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import ru.dexsys.helloandroid.domain.SettingsInteractor
import ru.dexsys.helloandroid.presentation.resourceproviders.SettingsResourceProvider
import javax.inject.Inject

abstract class SettingsPresenter<View> {
    abstract fun init(view: View)

    abstract suspend fun initSubscriptions()

    abstract fun onDestroy()

    abstract fun onThrowException(throwable: Throwable)

    abstract fun onButtonUiThreadProcessingClick()

    abstract fun onButtonIoThreadProcessingClick()

    abstract fun onButtonAsyncTaskProcessingClick()

    abstract suspend fun onButtonCoroutineProcessingClick()

    abstract suspend fun onButtonAsyncTwoParallelMethodCallsClick()

    abstract suspend fun onButtonCoroutineErrorClick()

    abstract suspend fun onButtonCoroutineFlowClick()

    abstract suspend fun onTextChanged(text: Editable?)
}

class SettingsPresenterImpl
@Inject constructor(
    private val resourceProvider: SettingsResourceProvider,
    private val interactor: SettingsInteractor
) : SettingsPresenter<SettingsView>() {

    private lateinit var view: SettingsView
    private var textChangeFlow = MutableSharedFlow<String>()

    override fun init(view: SettingsView) {
        this.view = view
        initState()
    }

    override suspend fun initSubscriptions() {
        textChangeFlow
            .debounce(500L)
            .collect {
                view.showToast(it)
            }
    }

    private fun initState() {
        view.setTitle(resourceProvider.dependencyInjectionInAction)
    }

    override fun onDestroy() {
        view.coroutineContext.cancelChildren()
    }

    override fun onThrowException(throwable: Throwable) {
        if (throwable is Exception) {
            view.showToast(throwable.message.orEmpty())
        }
    }

    override fun onButtonUiThreadProcessingClick() {
        showLoading()
        interactor.verySlowBusinessOperation { hideLoading() }
    }

    private fun showLoading() {
        view.showLoading()
    }

    private fun hideLoading() {
        view.hideLoading()
    }

    override fun onButtonIoThreadProcessingClick() {
        showLoading()
        interactor.verySlowBusinessOperationInIoThread { hideLoading() }
    }

    override fun onButtonAsyncTaskProcessingClick() {
        showLoading()
        interactor.verySlowBusinessOperationInAsyncTask { hideLoading() }
    }

    //await можно вызывать только внутри suspend функции, т.к. await является suspend функцией
    override suspend fun onButtonCoroutineProcessingClick() {
        showLoading()
        interactor.verySlowBusinessOperationInCoroutineAsync().await()
        hideLoading()
    }

    override suspend fun onButtonAsyncTwoParallelMethodCallsClick() {
        showLoading()
        val firstText = interactor.getFirstImportantStringAsync()
        val secondText = interactor.getSecondImportantStringAsync()
        val importantText = "${firstText.await()} ${secondText.await()}"
        hideLoading()
        view.showToast(importantText)
    }

    override suspend fun onButtonCoroutineErrorClick() {
        interactor.getMightyCrushingException()
    }

    //collect можно вызывать только внутри suspend функции, т.к. collect является suspend функцией
    override suspend fun onButtonCoroutineFlowClick() {
        interactor.getCountdownFlow()
            .collect {
                view.setButtonFlowText(it)
            }
        view.setButtonFlowText(resourceProvider.defaultFlowText)
    }

    override suspend fun onTextChanged(text: Editable?) {
        textChangeFlow.emit(text.toString())
    }
}