package ru.dexsys.helloandroid.presentation.resourceproviders

import android.content.Context
import ru.dexsys.helloandroid.R
import javax.inject.Inject

interface SettingsResourceProvider {
    val defaultFlowText: String
    val dependencyInjectionInAction: String
}

class ResourceProviderImpl @Inject constructor(context: Context) : SettingsResourceProvider {
    override val defaultFlowText: String by lazy { context.getString(R.string.helloandroid_coroutine_flow) }
    override val dependencyInjectionInAction: String by lazy { context.getString(R.string.helloandroid_dependency_injection_in_action) }
}