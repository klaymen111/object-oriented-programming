package ru.dexsys.helloandroid.presentation

import android.util.Log
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import ru.dexsys.helloandroid.domain.ChatInteractor
import ru.dexsys.helloandroid.presentation.databuilders.ChatDataBuilder
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

//Презентер отвечает только за логику отображения данных
class ChatPresenter
@Inject constructor(
    private val chatDataBuilder: ChatDataBuilder,
    private val chatInteractor: ChatInteractor
) : Presenter<ChatView>(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + CoroutineExceptionHandler { coroutineContext, throwable ->
            onThrowException(throwable)
        }

    private fun onThrowException(throwable: Throwable) {
        Log.d("onThrowException", throwable.message.orEmpty())
    }

    private lateinit var view: ChatView

    //Здесь происходит инициализация интерфейса view, через который презентер будет осуществлять показ
    override fun init(view: ChatView) {
        this.view = view
        initState()
    }

    //Здесь получаем данные из репозитория и используем их для создания сущностей для отображения
    private fun initState() {
        launch {
            chatInteractor.getMessagesAsync().collect {
                view.updateItems(chatDataBuilder.buildChatItems(it))
            }
        }
    }

    //Здесь нужно позаботиться о объектах, для которых критично наличие экземпляра View
    override fun onDestroy() {
    }
}
