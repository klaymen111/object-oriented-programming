package ru.dexsys.helloandroid.presentation.databuilders

import ru.dexsys.helloandroid.domain.vo.ChatMessageVO
import ru.dexsys.helloandroid.presentation.items.ChatItem
import ru.dexsys.helloandroid.presentation.items.IncomeMessageItem
import javax.inject.Inject

//DataBuilder занимается лишь сборкой сущностей для отображения
interface ChatDataBuilder {
    fun buildChatItems(messages: List<ChatMessageVO>): List<ChatItem>
}

class ChatDataBuilderImpl @Inject constructor() : ChatDataBuilder {
    override fun buildChatItems(messages: List<ChatMessageVO>) =
        messages.mapNotNull { if (it.isIncomeMessage) IncomeMessageItem(it.text) else null }
}
