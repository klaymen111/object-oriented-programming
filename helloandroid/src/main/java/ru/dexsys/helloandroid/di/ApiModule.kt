package ru.dexsys.helloandroid.di

import dagger.Binds
import dagger.Module
import ru.dexsys.helloandroid.HelloAndroidApi
import ru.dexsys.helloandroid.HelloAndroidApiImpl

//dagger модуль для внедрения api модуля HelloAndroid
@Module
interface ApiModule {
    @Binds
    fun bindsHelloAndroidApi(api: HelloAndroidApiImpl): HelloAndroidApi
}