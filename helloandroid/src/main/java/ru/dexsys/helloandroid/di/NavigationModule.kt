package ru.dexsys.helloandroid.di

import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.NavigatorHolder
import dagger.Module
import dagger.Provides
import ru.dexsys.helloandroid.di.scopes.ActivityScope
import ru.dexsys.helloandroid.navigation.routers.HelloRouter

@Module
class NavigationModule {
    @Provides
    @ActivityScope
    fun provideBaseRouter(): HelloRouter {
        return HelloRouter()
    }

    @Provides
    @ActivityScope
    fun provideCicerone(router: HelloRouter): Cicerone<HelloRouter> {
        return Cicerone.create(router)
    }

    @Provides
    @ActivityScope
    fun provideNavigatorHolder(cicerone: Cicerone<HelloRouter>): NavigatorHolder {
        return cicerone.getNavigatorHolder()
    }
}