package ru.dexsys.helloandroid.di

import android.content.Context
import androidx.room.Room
import dagger.Binds
import dagger.Module
import dagger.Provides
import ru.dexsys.helloandroid.data.ChatDataBase
import ru.dexsys.helloandroid.data.ChatRepository
import ru.dexsys.helloandroid.data.ChatRepositoryImpl
import ru.dexsys.helloandroid.data.converters.MessagesConverter
import ru.dexsys.helloandroid.data.converters.MessagesConverterImpl
import ru.dexsys.helloandroid.data.datasources.*
import ru.dexsys.helloandroid.di.scopes.ActivityScope

@Module(includes = [RepositoriesModule.InstanceModule::class])
interface RepositoriesModule {

    @ActivityScope
    @Binds
    fun provideChatRepository(impl: ChatRepositoryImpl): ChatRepository

    @ActivityScope
    @Binds
    fun provideLocalChatDataSource(impl: ChatLocalDataSourceImpl): ChatLocalDataSource

    @ActivityScope
    @Binds
    fun provideRemoteChatDataSource(impl: ChatRemoteDataSourceImpl): ChatRemoteDataSource

    @ActivityScope
    @Binds
    fun provideDBChatDataSource(impl: ChatDBDataSourceImpl): ChatDBDataSource

    @Binds
    fun provideMessagesConverter(impl: MessagesConverterImpl): MessagesConverter

    @Module
    class InstanceModule {
        @Provides
        fun providesChatDataBase(context: Context): ChatDataBase =
            Room.databaseBuilder(context, ChatDataBase::class.java, "chat-database")
                .build()
    }
}