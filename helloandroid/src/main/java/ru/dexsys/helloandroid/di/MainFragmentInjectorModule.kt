package ru.dexsys.helloandroid.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.dexsys.helloandroid.AwesomeFragment
import ru.dexsys.helloandroid.ViewAndViewGroupFragment

@Module
interface MainFragmentInjectorModule {
    @ContributesAndroidInjector()
    fun contributeAwesomeFragmentFactory(): AwesomeFragment

    @ContributesAndroidInjector()
    fun contributeViewAndViewGroupFragmentFactory(): ViewAndViewGroupFragment
}
