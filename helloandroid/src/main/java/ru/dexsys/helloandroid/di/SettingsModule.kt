package ru.dexsys.helloandroid.di

import dagger.Binds
import dagger.Module
import ru.dexsys.helloandroid.domain.SettingsInteractor
import ru.dexsys.helloandroid.domain.SettingsInteractorImpl
import ru.dexsys.helloandroid.presentation.SettingsPresenter
import ru.dexsys.helloandroid.presentation.SettingsPresenterImpl
import ru.dexsys.helloandroid.presentation.SettingsView
import ru.dexsys.helloandroid.presentation.resourceproviders.ResourceProviderImpl
import ru.dexsys.helloandroid.presentation.resourceproviders.SettingsResourceProvider

@Module
interface SettingsModule {
    //Аннотируются методы интерфейса выполняющие биндинг зависимостей
    @Binds
    fun bindsResourceProvider(impl: ResourceProviderImpl): SettingsResourceProvider

    @Binds
    fun bindsPresenter(impl: SettingsPresenterImpl): SettingsPresenter<SettingsView>

    @Binds
    fun bindsInteractor(impl: SettingsInteractorImpl): SettingsInteractor
}