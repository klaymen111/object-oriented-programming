package ru.dexsys.helloandroid.di

import dagger.Binds
import dagger.Module
import ru.dexsys.helloandroid.utils.analytics.AnalyticsManager
import ru.dexsys.helloandroid.utils.analytics.AnalyticsManagerImpl

@Module
interface FirebaseModule {
    @Binds
    fun bindsAnalyticsManager(impl: AnalyticsManagerImpl): AnalyticsManager
}