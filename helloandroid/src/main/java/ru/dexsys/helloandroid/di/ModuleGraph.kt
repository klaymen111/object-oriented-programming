package ru.dexsys.helloandroid.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import ru.dexsys.helloandroid.HelloAndroidComponent
import ru.dexsys.helloandroid.HelloAndroidDependencies
import javax.inject.Singleton

//Описание графа модуля

@Singleton
// Анноторирует объект, который инжектор создает только один раз
@Component(
    modules = [AndroidSupportInjectionModule::class,
        ApiModule::class,
        ApplicationModule::class,
        NetworkApiModule::class,
        FirebaseModule::class,
        HelloAndroidActivityInjectorModule::class],
    dependencies = [HelloAndroidDependencies::class]
)
// Аннотирует интерфейс или абстрактный класс, для которого должна быть сгенерирована полностью
// сформированная реализация с внедрением зависимостей из набора модулей
interface ModuleGraph : AndroidInjector<DaggerApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<DaggerApplication>() {
        @BindsInstance
        //Добавляем в зависимости application для последующего получения контекста
        abstract fun plus(application: Application): Builder
        //Добавляем внешние зависимости модуля
        abstract fun plus(dependencies: HelloAndroidDependencies): Builder
    }

    fun inject(component: HelloAndroidComponent)
}