package ru.dexsys.helloandroid.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.dexsys.helloandroid.BottomNavigationActivity
import ru.dexsys.helloandroid.MainActivity
import ru.dexsys.helloandroid.StartActivity
import ru.dexsys.helloandroid.di.scopes.ActivityScope

@Module
interface HelloAndroidActivityInjectorModule {
    // Данная аннотация служит для генерации AndroidInjector для возвращаемого типа метода, над
    // которым указана аннотация. Данный AndroidInjector является сабкомпонентом. Этот сабкомпонент
    // является дочерним того компонента (или сабкомпонента), в который данный модуль будет добавлен
    @ContributesAndroidInjector()
    fun contributeInStartActivity(): StartActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainFragmentInjectorModule::class, NavigationModule::class])
    fun contributeInMainActivity(): MainActivity

    // Аннотация содержит параметр modules. Данный параметр указывает на то, какие модули будут
    // добавлены к данному сгенерированному сабкомпоненту.
    @ActivityScope
    @ContributesAndroidInjector(modules = [BottomNavigationFragmentInjectorModule::class, RepositoriesModule::class])
    fun contributeInBottomNavigationActivity(): BottomNavigationActivity
}