package ru.dexsys.helloandroid.di

import dagger.Binds
import dagger.Module
import ru.dexsys.helloandroid.domain.ChatInteractor
import ru.dexsys.helloandroid.domain.ChatInteractorImpl
import ru.dexsys.helloandroid.presentation.ChatPresenter
import ru.dexsys.helloandroid.presentation.ChatView
import ru.dexsys.helloandroid.presentation.Presenter
import ru.dexsys.helloandroid.presentation.databuilders.ChatDataBuilder
import ru.dexsys.helloandroid.presentation.databuilders.ChatDataBuilderImpl

@Module
interface ChatModule {
    //Аннотируются методы интерфейса выполняющие биндинг зависимостей
    @Binds
    fun provideChatPresenter(impl: ChatPresenter): Presenter<ChatView>

    @Binds
    fun provideChatDataBuilder(impl: ChatDataBuilderImpl): ChatDataBuilder

    @Binds
    fun providesChatInteractor(impl: ChatInteractorImpl): ChatInteractor
}