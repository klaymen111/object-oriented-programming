package ru.dexsys.helloandroid.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.dexsys.helloandroid.presentation.ChatFragment
import ru.dexsys.helloandroid.presentation.SettingsFragment

@Module
interface BottomNavigationFragmentInjectorModule {
    // Данная аннотация служит для генерации AndroidInjector для возвращаемого типа метода, над
    // которым указана аннотация. Данный AndroidInjector является сабкомпонентом. Этот сабкомпонент
    // является дочерним того компонента (или сабкомпонента), в который данный модуль будет добавлен
    @ContributesAndroidInjector(modules = [SettingsModule::class])
    fun contributeSettingsFragmentFactory(): SettingsFragment

    @ContributesAndroidInjector(modules = [ChatModule::class])
    fun contributeChatFragmentFactory(): ChatFragment
}