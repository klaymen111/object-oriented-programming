package ru.dexsys.helloandroid.di

import android.util.Log
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.dexsys.helloandroid.data.NetworkApi
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val BASE_URL = "https://60a3f74afbd48100179db50e.mockapi.io/api/v1/"
private const val LOG_OKHTTP_TAG = "OkHttp"
private const val HTTP_TIMEOUT = 60L

@Module
class NetworkApiModule {

    // OkHttp - это клиент HTTP / SPDY
    @Singleton
    @Provides
    fun providesOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient.Builder()
            // можно добавлять любое количество перехватчиков
            .addInterceptor(httpLoggingInterceptor)
            // таймаут чтения
            .readTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
            // таймаут подключения
            .connectTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
            // здесь можно настроить dns, proxy, ping interval и так далее
            .build()

    // Retrofit - обертка для OkHttp или как привычно слышать REST API. Retrofit генерирует URL,
    // по уже заданным правилам
    @Singleton
    @Provides
    fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            // здесь указываем базовый uri для генерации ссылок retrofit
            .baseUrl(BASE_URL)
            // внедряем наш okhttpClient
            .client(okHttpClient)
            // здесь вы задаете фабрику для сериализации/десериализации. Если стандартный gson не
            // удовлетворяет вашим запросам, это то место где вы можете внедрить свой gson
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun providesNetworkApi(retrofit: Retrofit): NetworkApi =
        // генерируем имплементацию нашего REST API с помощью retrofit
        retrofit.create(NetworkApi::class.java)

    @Singleton
    @Provides
    fun provideLogger(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                Log.d(LOG_OKHTTP_TAG, message)
            }
        })
            .setLevel(HttpLoggingInterceptor.Level.BODY)
    }
}