package ru.dexsys.helloandroid.di.scopes

import javax.inject.Scope

@Scope
annotation class ActivityScope