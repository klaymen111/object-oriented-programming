package ru.dexsys.helloandroid.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

// AndroidInjectionModule::class Содержит биндинги для удобства использования классов фреймворка dagger.android
// includes - включает одни модули в составв других
@Module(includes = [AndroidInjectionModule::class])
class ApplicationModule {
    // Аннотирует биндинг ресурса через реализацию метода (можно что-то инстанциировать прямо в теле метода)
    @Provides
    // Анноторирует объект, который инжектор создает только один раз
    @Singleton
    fun provideApplicationContext(application: Application): Context {
        return application.applicationContext
    }
}