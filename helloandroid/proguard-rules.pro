# Proguard — это open-source утилита для оптимизации и обфускации Java кода.
# Этот инструмент обрабатывает уже скомпилированный Java код, т.е. байткод.
# Все манипуляции Proguard-а с байткодом можно разделить на 3 основных категории:
# Code shrinking, Optimisation и Obfuscation.

#Code shrinking
# Proguard удаляет весь неиспользуемый код (Guava, Apache Commons, Google Play Services
# и т.д.) и сокращает размер приложения

#Optimisation
# Proguard умеет выполнять peephole-оптимизации, уменьшать количество переменных,
# упрощать хвостовые рекурсии и и.т.д. Помимо таких общих операций, у Proguard есть оптимизации,
# полезные именно для Android-платформы, например, замена enum классов int-ами, удаление логгирующих инструкций

# Obfuscation
# Сакращает названия классов, методов, переменных до возможного минимума, делая их трудночитаемыми
# для человека. Усложнаяет декомпиляцию

# Proguard генерирует 4 файла
# seeds.txt - точки входа в приложени
# unusage.txt - то что не используется и было удалено
# mapping.txt - мэпинг для деобфускации
# dump.txt - дамп кода перед упаковкой

# Конфигурация:
# -keep public class com.example.MyActivity
# сохранить класс com.example.MyActivity
#
# -keep public class * extends android.app.Activity
# сохранить все публичные классы, наследующие android.app.Activity

# -dontwarn java.lang.management.** - отключить предупреждение Proguard